interface NetworkConfig {
  tokenAddresses: {
    usdt: string;
    usdc: string;
  };
  accounts: {
    proxyAdmin: string;
    admin: string;
    minter: string;
    burner: string;
    billing: string;
    destinationWallet: string;
    refunder: string;
  };
  proxyAddresses: {
    accessControl: string;
    billing: string;
    nodeIdNFT: string;
    subscriptions: string;
    wrapper: string;
  };
}

const DEPLOY_CONFIGS: Record<string, NetworkConfig> = {
  mainnet: {
    accounts: {
      // deployer: 0xedEc96c0C1F3580e994Dc76BAA7169Be59Fc6c08
      admin: "0x1fC01374A2A2375cFa93478c2bE459F98A6355d8", // safe address
      billing: "0x1fC01374A2A2375cFa93478c2bE459F98A6355d8", // safe address
      burner: "0x1fC01374A2A2375cFa93478c2bE459F98A6355d8", // safe address
      destinationWallet: "0x1fC01374A2A2375cFa93478c2bE459F98A6355d8", // safe address
      proxyAdmin: "0x1fC01374A2A2375cFa93478c2bE459F98A6355d8", // safe address
      minter: "0x6291864924C7B061D5930D1c3e05D586804d0807", // backend account address accounts[0]
      refunder: "0xBd0BF90473bd7e635c641656fA36c7e1825bF81A", // backend account address accounts[1]
    },
    tokenAddresses: {
      usdc: "0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E",
      usdt: "0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7",
    },
    proxyAddresses: {
      accessControl: "0x8BD06F20766E48Da514370056aA72070aE57882b",
      billing: "0xFF9a9cA3C1A49F74aB5D8233f11f7531f2Ef63c8",
      nodeIdNFT: "0x7C98CBd78b2180659484B91b06728BF8ABfde3BB",
      subscriptions: "0x59a90cD4fa3f6F9544fb26EEeE913a35d6E7772e",
      wrapper: "0x769Fc9b5038d8843895b50a904e04b58b0d4a9CB",
    },
  },
  fuji: {
    accounts: {
      // deployer: 0xA22Dac31C92BB073F80333caFD9c404BDA6A729F
      admin: "0xe61e3516e98667A1c79F067DDeDB9005D911CF65",
      billing: "0xe61e3516e98667A1c79F067DDeDB9005D911CF65",
      burner: "0xe61e3516e98667A1c79F067DDeDB9005D911CF65",
      destinationWallet: "0xe61e3516e98667A1c79F067DDeDB9005D911CF65",
      proxyAdmin: "0xe61e3516e98667A1c79F067DDeDB9005D911CF65",
      minter: "0x89f18ddF8B0b03bfc4D579E7df3f279699C6C988", // backend account address accounts[0]
      refunder: "0xD24783C1bFAd3fF4d4b5fC27b518483C5273F64E", // backend account address accounts[1]
    },
    tokenAddresses: {
      usdc: "0x9553fD4EB766D4c93011760F270a9F6Bb3B226D0",
      usdt: "0xe77Cf8201Bc7dffcE0cb5ac1a1821c473D3CC32c",
    },
    proxyAddresses: {
      accessControl: "0x89a89fE9B973E0474e983FD4baE5Ac09634fE598",
      billing: "0xa05D8018B51A97b1b403A4630876317260e72B69",
      nodeIdNFT: "0x85644e02E5eD82AA4ed37f231068Bc30E2C5C7a1",
      subscriptions: "0x2743A3c6B1618AF4335559165c6D992d02cf2846",
      wrapper: "0xF82364d989A87791Ac2C6583B85DbC56DE7F2cf5",
    },
  },
  montblanc: {
    accounts: {
      admin: "0x99E1E5b2dA7246Eb99986eFda6D36B37Cbc2eB65", // accounts[1]
      billing: "0xEBBe2fAbDb41a0D79208cA3c2f3f14454a696A5c", // accounts[2]
      burner: "0x9477474b90fF28c54394955076A956DDCDD53289", // accounts[3]
      destinationWallet: "0x7F367e91399b54c9435D73dCA6F158ea60957eEb", // accounts[4]
      minter: "0x710b28D322613132584639dc4A510289f43EBBEB", // accounts[5]
      proxyAdmin: "0x1ca27077Aa21682A2F0E72aF8523800bEaE829bA", // accounts[6]
      refunder: "0x65410e074020D47250612f5A853555CF875DdCc6", // accounts[7]
    },
    tokenAddresses: {
      usdc: "0x9553fD4EB766D4c93011760F270a9F6Bb3B226D0",
      usdt: "0x31EAe79B6525Ab9644a27a69e9e25e731483ADdd",
    },
    proxyAddresses: { // no proxy address registered for montblanc network (redeployed from scratch)
      accessControl: "",
      billing: "",
      nodeIdNFT: "",
      subscriptions: "",
      wrapper: "",
    },
  },
};

export { DEPLOY_CONFIGS, NetworkConfig };
