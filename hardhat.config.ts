import { HardhatUserConfig } from "hardhat/config";
import { env } from 'node:process';
import "@openzeppelin/hardhat-upgrades";
import "@openzeppelin/hardhat-defender";
import "@nomicfoundation/hardhat-toolbox";
import "hardhat-gas-reporter";
import "solidity-docgen";
require('dotenv').config();

const config: HardhatUserConfig = {
  solidity: { version: "0.8.17", settings: { optimizer: { enabled: true, runs: 200 } } },
  defender: {
    apiKey: env.DEFENDER_TEAM_API_KEY,
    apiSecret: env.DEFENDER_TEAM_API_SECRET_KEY
  },
  etherscan: {
    apiKey: {
      avalanche: env.SNOWSCAN_API_KEY,
      avalancheFujiTestnet: env.SNOWSCAN_API_KEY,
    }
  },
  docgen: {
    output: 'docs',
    pages: () => 'api.md',
  },
  networks: {
    // Hardhat is a fork of mainnet
    hardhat: {
      forking: {
        url: "https://api.avax.network/ext/bc/C/rpc",
      },
    },
    // Mainnet testnet: https://snowtrace.io
    mainnet: {
      url: "https://api.avax.network/ext/bc/C/rpc",
      chainId: 43114,
      accounts: {
        mnemonic: env.SEED,
        count: 20,
      },
    },
    // fuji testnet: https://testnet.snowtrace.io
    fuji: {
      url: "https://api.avax-test.network/ext/bc/C/rpc",
      chainId: 43113,
      accounts: {
        mnemonic: env.SEED,
        count: 20,
      },
    },
    montblanc: {
      url: "https://rpc.devnet.oonodz.network/ext/bc/C/rpc",
      chainId: 43112,
      accounts: {
        mnemonic: env.SEED,
        count: 20,
      },
    },
  },
  gasReporter: {
    enabled: true,
    token: "AVAX",
    currency: "EUR",
    coinmarketcap: "f643626f-0558-46d9-8e93-0a6efb110b19",
    gasPriceApi: "https://api.snowtrace.io/api?module=proxy&action=eth_gasPrice",
  },
};

export default config;
