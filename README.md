# Avalanche Staking Service - Smart Contracts

## Architecture

The implementation is built based on 4 Smart Contracts:

- `AccessControl.sol`:
  The objective of this contract is to manage the different roles of the wallets used in other contracts of the project.
  It allows one or more admins to assign or revoke roles to other wallets.
  All Smart Contracts using this Access Role management model must inherit `RoleBasedAccessControlled`

- `NodeIdNFT.sol`:
  This Smart Contract is a ERC721 "NFT" Contract, that represents the ownership of a NodeID.
  It uses 2 roles:
  * MINTER : wallets that are authorized to mint new tokens
  * BURNER : wallets authorized to burn existing token (token's owner is also authorized his own token).

- `Billing.sol`:
  This Smart Contract manages the billing part: supported currencies, exchange rates to USD (for the moment only USD-stable ERC20 tokens are supported), VAT rate.
  This Smart Contract defines a role: BILLING. Each wallet with BILLING role can add/remove a currency in the list of supported currency, and manage the list of VAT rate per country of residence.

- `Subscription.sol`:
  This is the main Smart Contract that receives subscriptions from customer.
  the overall subscription process is:

  - If customer is not yet registered, he must send a transaction to `register` function with his country of residence
  - Once customer is registered, he can send a transaction to `newSubscription` function to create a new subscription

  Before calling `newSubscription`, customer has to allow `Subscription` contract to transfer funds from his (ERC20) account, using the `allowance` function of ERC20.
  Funds received by `Subscription` contract are immediately transfered to a `destinationWallet` configured on `Subscription` contract. Wallets with ADMIN role can change the `destinationWallet`.

## Roles

- `ADMIN_ROLE` : allowed to manage grant / revoke roles for other wallets. Allowed to change the `destinationWallet` of `Subscription` Smart Contract.
- `MINTER_ROLE`: allowed to mint new NodeIdNFT tokens
- `BURNER_ROLE`: allowed to burn existing NodeIdNFT tokens
- `BILLING_ROLE`: allowed to manage the list of supported currencies & VAT rates
- `REFUNDER_ROLE`: allowed to refund users for unused periods

## Implementation details

- Project is using [Hardhat](http://hardhat.org) for compiling, testing and deploying Smart Contracts.
- [Solhint](https://github.com/protofire/solhint) is the linter.
- [OpenZeppelin](https://www.openzeppelin.com/contracts) library is used to define standard Smart Contracts (ERC20, ERC721, AccessControl)
- All these Smart Contracts are Upgradeable using [OpenZeppelin Upgradeable patterns & plugins](https://docs.openzeppelin.com/upgrades-plugins/1.x/writing-upgradeable)
- `hardhat-gas-reporter` plugin is used to compute gas fees for all operations

## Build & test contracts

```
# Clone repository, and then run in the project folder:

# Install dependencies
npm install

# Build Smart Contracts
npm run compile

# Run linter
npm run lint

# Run unit tests
npm run test
```

## Deployment

### Fuji testnet

```
npm run deploy:fuji
npm run transferOwnership:fuji
```

### Mainnet

```
npm run deploy:mainnet
npm run transferOwnership:mainnet
```

## Testnet contracts

USDX contracts on fuji:
- USDC: 0x08123E97291e1562eD2fC12C1f3325c6c4783a77
- USDT: 0x7B8FDF47e587BaF614213f31A56E17B5C984C119

Contract addresses:
- AccessControl: [0x89a89fE9B973E0474e983FD4baE5Ac09634fE598](https://testnet.snowtrace.io/address/0x89a89fE9B973E0474e983FD4baE5Ac09634fE598)
- Billing: [0xa05D8018B51A97b1b403A4630876317260e72B69](https://testnet.snowtrace.io/address/0xa05D8018B51A97b1b403A4630876317260e72B69)
- NodeIdNFT: [0x85644e02E5eD82AA4ed37f231068Bc30E2C5C7a1](https://testnet.snowtrace.io/address/0x85644e02E5eD82AA4ed37f231068Bc30E2C5C7a1)
- Subscriptions: [0x2743A3c6B1618AF4335559165c6D992d02cf2846](https://testnet.snowtrace.io/address/0x2743A3c6B1618AF4335559165c6D992d02cf2846)
- Wrapper: [0xF82364d989A87791Ac2C6583B85DbC56DE7F2cf5](https://testnet.snowtrace.io/address/0xF82364d989A87791Ac2C6583B85DbC56DE7F2cf5)

## Mainnet contracts

USDX contracts on mainnet:
- USDC: 0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E
- USDT: 0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7

Contract addresses:
- AccessControl: [0x8BD06F20766E48Da514370056aA72070aE57882b](https://snowtrace.io/address/0x8BD06F20766E48Da514370056aA72070aE57882b)
- Billing: [0xFF9a9cA3C1A49F74aB5D8233f11f7531f2Ef63c8](https://snowtrace.io/address/0xFF9a9cA3C1A49F74aB5D8233f11f7531f2Ef63c8)
- NodeIdNFT: [0x7C98CBd78b2180659484B91b06728BF8ABfde3BB](https://snowtrace.io/address/0x7C98CBd78b2180659484B91b06728BF8ABfde3BB)
- Subscriptions: [0x59a90cD4fa3f6F9544fb26EEeE913a35d6E7772e](https://snowtrace.io/address/0x59a90cD4fa3f6F9544fb26EEeE913a35d6E7772e)
