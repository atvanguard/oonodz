import { readFileSync, writeFileSync, promises as fsPromises } from 'fs';
import { join, resolve } from 'path';

function extractAbi(contract: string) {
  try {
    const dir = resolve(
      __dirname,
      "../artifacts/contracts/" + contract + ".sol/" + contract + ".json"
    )
    const file = readFileSync(dir, "utf8")
    const json = JSON.parse(file)
    const abi = json.abi

    writeFileSync(join(__dirname, "../abi/" + contract + ".abi.json"), JSON.stringify(abi), {
      flag: 'w',
    });

  } catch (e) {
    console.log(`e`, e)
  }
}

async function main() {
  extractAbi("AccessControl");
  extractAbi("Billing");
  extractAbi("NodeIdNFT");
  extractAbi("Subscriptions");
  extractAbi("Wrapper");
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
