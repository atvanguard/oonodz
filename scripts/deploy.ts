import { ethers, upgrades, network } from "hardhat";
import { DEPLOY_CONFIGS, NetworkConfig } from "../deploy-config";
import { readFileSync, writeFileSync, promises as fsPromises } from 'fs';
import { resolve } from 'path';
import { join } from 'path';

function validateConfig(config: NetworkConfig) {
  if (!ethers.utils.isAddress(config.tokenAddresses.usdc))
    throw new Error("Invalid config: tokenAddresses.usdc is not a valid address");
  if (!ethers.utils.isAddress(config.tokenAddresses.usdt))
    throw new Error("Invalid config: tokenAddresses.usdt is not a valid address");

  if (!ethers.utils.isAddress(config.accounts.proxyAdmin))
    throw new Error("Invalid config: config.accounts.proxyAdmin is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.admin))
    throw new Error("Invalid config: config.accounts.admin is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.burner))
    throw new Error("Invalid config: config.accounts.burner is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.minter))
    throw new Error("Invalid config: config.accounts.minter is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.billing))
    throw new Error("Invalid config: config.accounts.billing is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.destinationWallet))
    throw new Error("Invalid config: config.accounts.destinationWallet is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.refunder))
    throw new Error("Invalid config: config.accounts.refunder is not a valid address");
}

async function main() {
  const { name } = network;
  console.log(`Deploying to network: ${name}`);
  const config = DEPLOY_CONFIGS[name];
  if (!config) {
    throw new Error(`Cannot load config for network ${name}`);
  }

  console.log(`
    - USDC: ${config.tokenAddresses.usdc}
    - USDT: ${config.tokenAddresses.usdt}
  `);

  validateConfig(config);

  const [deployer] = await ethers.getSigners();

  const deployerAddress = await deployer.getAddress();

  // Deploy AccessControl contract
  const AccessControl = await ethers.getContractFactory("AccessControl");
  const access = await upgrades.deployProxy(AccessControl, [], { timeout: 300000 });
  await access.deployed();

  // Initilize accounts
  await (await access.grantRole(access.ADMIN_ROLE(), config.accounts.admin)).wait();
  await (await access.grantRole(access.BILLING_ROLE(), config.accounts.billing)).wait();
  await (await access.grantRole(access.BURNER_ROLE(), config.accounts.burner)).wait();
  await (await access.grantRole(access.MINTER_ROLE(), config.accounts.minter)).wait();
  await (await access.grantRole(access.REFUNDER_ROLE(), config.accounts.refunder)).wait();

  // Deploy Billing contract
  const Billing = await ethers.getContractFactory("Billing");
  const billing = await upgrades.deployProxy(Billing, [
    access.address,
    config.tokenAddresses.usdt,
    config.tokenAddresses.usdc,
  ], { timeout: 300000 });
  await billing.deployed();

  // Deploy NodeIdNFT contract
  const NodeIdNFT = await ethers.getContractFactory("NodeIdNFT");
  const nft = await upgrades.deployProxy(NodeIdNFT, [access.address], { timeout: 300000 });
  await nft.deployed();

  // Deploy Subscription contract
  const Subscriptions = await ethers.getContractFactory("Subscriptions");
  const subscriptions = await upgrades.deployProxy(Subscriptions, [
    access.address,
    billing.address,
    nft.address,
    config.accounts.destinationWallet,
  ], { timeout: 300000 });
  await subscriptions.deployed();

  await nft.connect(deployer).setSubscriptions(subscriptions.address);

  // Remove deployer admin's role
  await access.renounceRole(access.ADMIN_ROLE(), deployerAddress);

  console.log(`Contracts deployed!
    - AccessControl: ${access.address}
    - Billing: ${billing.address}
    - NodeIdNFT: ${nft.address}
    - Subscriptions: ${subscriptions.address}
  `);

  try {
    const contractsAddr: JSON = <JSON><unknown>{
      "AccessControl": access.address,
      "Billing": billing.address,
      "NodeIdNFT": nft.address,
      "Subscriptions": subscriptions.address,
    };

    writeFileSync(join(__dirname, "../addr.json"), JSON.stringify(contractsAddr), {
      flag: 'w',
    });

  } catch (e) {
    console.log(`e`, e);
  }
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
