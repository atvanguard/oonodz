import { ethers, upgrades, network } from "hardhat";
import { DEPLOY_CONFIGS, NetworkConfig } from "../deploy-config";
import { readFileSync, writeFileSync, promises as fsPromises } from 'fs';
import { resolve } from 'path';
import { join } from 'path';

function validateConfig(config: NetworkConfig) {
  if (!ethers.utils.isAddress(config.tokenAddresses.usdc))
    throw new Error("Invalid config: tokenAddresses.usdc is not a valid address");
  if (!ethers.utils.isAddress(config.tokenAddresses.usdt))
    throw new Error("Invalid config: tokenAddresses.usdt is not a valid address");

  if (!ethers.utils.isAddress(config.accounts.proxyAdmin))
    throw new Error("Invalid config: config.accounts.proxyAdmin is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.admin))
    throw new Error("Invalid config: config.accounts.admin is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.burner))
    throw new Error("Invalid config: config.accounts.burner is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.minter))
    throw new Error("Invalid config: config.accounts.minter is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.billing))
    throw new Error("Invalid config: config.accounts.billing is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.destinationWallet))
    throw new Error("Invalid config: config.accounts.destinationWallet is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.refunder))
    throw new Error("Invalid config: config.accounts.refunder is not a valid address");
}

async function main() {
  const { name } = network;
  console.log(`Deploying to network: ${name}`);
  const config = DEPLOY_CONFIGS[name];
  if (!config) {
    throw new Error(`Cannot load config for network ${name}`);
  }

  console.log(`
    - USDC: ${config.tokenAddresses.usdc}
    - USDT: ${config.tokenAddresses.usdt}
  `);

  validateConfig(config);

  const [deployer, other] = await ethers.getSigners();

  const deployerAddress = await deployer.getAddress();
  const otherAddress = await other.getAddress();

  console.log(`${deployerAddress}`);
  console.log(`${otherAddress}`);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
