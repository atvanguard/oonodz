import { ethers, upgrades, network } from "hardhat";
import { DEPLOY_CONFIGS, NetworkConfig } from "../deploy-config";
import { env } from 'node:process';
const { defender } = require("hardhat");
require('dotenv').config();

async function upgrade() {

    const { name } = network;
    console.log(`Deploying to network: ${name}`);
    const config = DEPLOY_CONFIGS[name];
    if (!config) {
        throw new Error(`Cannot load config for network ${name}`);
    }

    const [deployer] = await ethers.getSigners();
    const deployerAddress = await deployer.getAddress();

    console.log(`${deployerAddress}`);
    console.log(`${env.DEFENDER_TEAM_API_KEY}`);


    // WARNING: This is an example. DO NOT USE IT ON PRODUCTION YET!
    console.log("Upgrading AccessControl...");
    const Access = await ethers.getContractFactory("AccessControl");
    const accessProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['accessControl'];
    const accessProposal = await defender.proposeUpgrade(accessProxyAddress, Access);
    console.log("Upgrade proposal created at:", accessProposal.url);

    console.log("Upgrading Billing...");
    const Billing = await ethers.getContractFactory("Billing");
    const billingProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['billing'];
    const billingProposal = await defender.proposeUpgrade(billingProxyAddress, Billing);
    console.log("Upgrade proposal created at:", billingProposal.url);

    console.log("Upgrading NodeIdNFT...");
    const NodeIdNFT = await ethers.getContractFactory("NodeIdNFT");
    const NodeIdNFTProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['nodeIdNFT'];
    const NodeIdNFTProposal = await defender.proposeUpgrade(NodeIdNFTProxyAddress, NodeIdNFT);
    console.log("Upgrade proposal created at:", NodeIdNFTProposal.url);

    console.log("Upgrading Subscription...");
    const Subscriptions = await ethers.getContractFactory("Subscriptions");
    const proxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['subscriptions'];
    const proposal = await defender.proposeUpgrade(proxyAddress, Subscriptions);
    console.log("Upgrade proposal created at:", proposal.url);

    console.log("Upgrading Wrapper...");
    const Wrapper = await ethers.getContractFactory("Wrapper");
    const wrapperProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['wrapper'];
    const wrapper = await defender.proposeUpgrade(wrapperProxyAddress, Wrapper);
    console.log("Upgrade proposal created at:", wrapper.url);
}

async function main() {
    upgrade();
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});