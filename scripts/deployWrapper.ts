import { ethers, upgrades, network } from "hardhat";
import { DEPLOY_CONFIGS, NetworkConfig } from "../deploy-config";

function validateConfig(config: NetworkConfig) {
  if (!ethers.utils.isAddress(config.tokenAddresses.usdc))
    throw new Error("Invalid config: tokenAddresses.usdc is not a valid address");
  if (!ethers.utils.isAddress(config.tokenAddresses.usdt))
    throw new Error("Invalid config: tokenAddresses.usdt is not a valid address");

  if (!ethers.utils.isAddress(config.accounts.proxyAdmin))
    throw new Error("Invalid config: config.accounts.proxyAdmin is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.admin))
    throw new Error("Invalid config: config.accounts.admin is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.burner))
    throw new Error("Invalid config: config.accounts.burner is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.minter))
    throw new Error("Invalid config: config.accounts.minter is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.billing))
    throw new Error("Invalid config: config.accounts.billing is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.destinationWallet))
    throw new Error("Invalid config: config.accounts.destinationWallet is not a valid address");
  if (!ethers.utils.isAddress(config.accounts.refunder))
    throw new Error("Invalid config: config.accounts.refunder is not a valid address");
}

async function main() {
  const { name } = network;
  console.log(`Deploying to network: ${name}`);
  const config = DEPLOY_CONFIGS[name];
  if (!config) {
    throw new Error(`Cannot load config for network ${name}`);
  }

  console.log(`
    - USDC: ${config.tokenAddresses.usdc}
    - USDT: ${config.tokenAddresses.usdt}
  `);

  validateConfig(config);

  const [deployer] = await ethers.getSigners();

  const deployerAddress = await deployer.getAddress();

  // Deploy Subscription contract
  const Wrapper = await ethers.getContractFactory("Wrapper");

  const accessProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['accessControl'];
  const subscriptionsAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['subscriptions'];
  const NodeIdNFTProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['nodeIdNFT'];
  const billingProxyAddress = DEPLOY_CONFIGS[name]['proxyAddresses']['billing'];
  const wrapper = await upgrades.deployProxy(Wrapper, [
    accessProxyAddress,
    subscriptionsAddress,
    NodeIdNFTProxyAddress,
    billingProxyAddress
  ], { timeout: 300000 });
  await wrapper.deployed();

  console.log(`Contracts deployed!
    - Wrapper: ${wrapper.address}
  `);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
