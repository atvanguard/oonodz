import { ethers, upgrades, network } from "hardhat";
import { DEPLOY_CONFIGS, NetworkConfig } from "../deploy-config";

async function main() {
  const { name } = network;
  console.log(`Deploying to network: ${name}`);
  const config = DEPLOY_CONFIGS[name];
  if (!config) {
    throw new Error(`Cannot load config for network ${name}`);
  }

  console.log("Transferring ownership of ProxyAdmin...");

  // The owner of the ProxyAdmin can upgrade our contracts
  await upgrades.admin.transferProxyAdminOwnership(config.accounts.proxyAdmin);
  console.log("Transferred ownership of ProxyAdmin to:", config.accounts.proxyAdmin);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});