import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { any } from "hardhat/internal/core/params/argumentTypes";
import { time } from "@nomicfoundation/hardhat-network-helpers";

const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");

const USDT_TOKEN = "0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7";
const USDC_TOKEN = "0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E";

describe("Subscription", function () {
  async function deploySubscriptionFixture() {
    const [deployer, admin, billing, burner, destinationWallet, minter, proxyAdmin, refunder, subscriber, destination, otherAccount, money] =
      await ethers.getSigners();
    const addresses = {
      deployer: await deployer.getAddress(),
      admin: await admin.getAddress(),
      billing: await billing.getAddress(),
      minter: await minter.getAddress(),
      burner: await burner.getAddress(),
      subscriber: await subscriber.getAddress(),
      destination: await destination.getAddress(),
      otherAccount: await otherAccount.getAddress(),
      money: await money.getAddress(),
      refunder: await refunder.getAddress(),
    };

    const AccessControl = await ethers.getContractFactory("AccessControl");
    const access = await AccessControl.deploy();

    const roles = {
      ADMIN: await access.ADMIN_ROLE(),
      BURNER: await access.BURNER_ROLE(),
      MINTER: await access.MINTER_ROLE(),
      BILLING: await access.BILLING_ROLE(),
      REFUNDER: await access.REFUNDER_ROLE(),
      WRAPPER: await access.WRAPPER_ROLE(),
    };

    await access.initialize();
    await access.grantRole(roles.ADMIN, addresses.admin);
    await access.renounceRole(roles.ADMIN, addresses.deployer);
    await access.connect(admin).grantRole(roles.BILLING, addresses.billing);
    await access.connect(admin).grantRole(roles.BURNER, addresses.burner);
    await access.connect(admin).grantRole(roles.MINTER, addresses.minter);
    await access.connect(admin).grantRole(roles.REFUNDER, addresses.refunder);

    const Billing = await ethers.getContractFactory("Billing");
    const billingContract = await Billing.deploy();
    await billingContract.initialize(access.address, USDT_TOKEN, USDC_TOKEN);

    const NFT = await ethers.getContractFactory("NodeIdNFT");
    const nft = await NFT.deploy();

    await nft.initialize(access.address);

    const Subscriptions = await ethers.getContractFactory("Subscriptions");
    const subscriptions = await Subscriptions.deploy();
    await subscriptions.initialize(access.address, billingContract.address, nft.address, addresses.destination);

    const cautionPrice = await nft.cautionPrice(); // get default caution price
    await nft.connect(admin).setSubscriptions(subscriptions.address); // Set caution price

    const usdtToken = await ethers.getContractAt("IERC20Upgradeable", USDT_TOKEN);
    const usdcToken = await ethers.getContractAt("IERC20Upgradeable", USDC_TOKEN);

    // Transfer 1.000.000 USDC to money account
    await usdcToken
      .connect(await ethers.getImpersonatedSigner("0x9f8c163cBA728e99993ABe7495F06c0A3c8Ac8b9"))
      .transfer(addresses.money, 1000000000000);

    // Transfer 1.000.000 USDT to money account
    await usdtToken
      .connect(await ethers.getImpersonatedSigner("0x9f8c163cBA728e99993ABe7495F06c0A3c8Ac8b9"))
      .transfer(addresses.money, 1000000000000);

    return {
      billingContract,
      subscriptions,
      nft,
      roles,
      subscriber,
      addresses,
      deployer,
      admin,
      billing,
      minter,
      burner,
      destination,
      otherAccount,
      usdtToken,
      usdcToken,
      money,
      cautionPrice,
      refunder
    };
  }

  describe("Destination wallet", function () {
    it("Shoud allow admin to change destination wallet", async function () {
      const { subscriptions, admin, addresses } = await loadFixture(deploySubscriptionFixture);
      expect(await subscriptions.destinationWallet()).to.equals(addresses.destination);

      await expect(subscriptions.connect(admin).changeDestationWallet(addresses.otherAccount))
        .to.emit(subscriptions, "DestinationWalletChanged")
        .withArgs(addresses.destination, addresses.otherAccount);
      expect(await subscriptions.destinationWallet()).to.equals(addresses.otherAccount);
    });

    it("Shoud revert if change destination wallet to same value", async function () {
      const { subscriptions, admin, addresses } = await loadFixture(deploySubscriptionFixture);
      expect(await subscriptions.destinationWallet()).to.equals(addresses.destination);

      await expect(subscriptions.connect(admin).changeDestationWallet(addresses.destination)).to.be.revertedWith(
        "same wallet"
      );
    });

    it("Shoud allow ONLY admin to change destination wallet", async function () {
      const { subscriptions, deployer, billing, minter, burner, addresses } = await loadFixture(
        deploySubscriptionFixture
      );
      expect(await subscriptions.destinationWallet()).to.equals(addresses.destination);

      await expect(subscriptions.connect(deployer).changeDestationWallet(addresses.destination)).to.be.revertedWith(
        "not an admin"
      );
      await expect(subscriptions.connect(billing).changeDestationWallet(addresses.destination)).to.be.revertedWith(
        "not an admin"
      );
      await expect(subscriptions.connect(minter).changeDestationWallet(addresses.destination)).to.be.revertedWith(
        "not an admin"
      );
      await expect(subscriptions.connect(burner).changeDestationWallet(addresses.destination)).to.be.revertedWith(
        "not an admin"
      );
    });
  });

  describe("Customer", function () {
    it("Should register new customer", async function () {
      const { subscriptions, subscriber, addresses } = await loadFixture(deploySubscriptionFixture);
      expect(await subscriptions.customerExists(addresses.subscriber)).to.false;

      await expect(subscriptions.connect(subscriber)["register(uint16)"](250))
        .to.emit(subscriptions, "NewCustomerRegistered")
        .withArgs(addresses.subscriber, 250);
      expect(await subscriptions.customerExists(addresses.subscriber)).to.true;
      expect((await subscriptions.customers(addresses.subscriber)).countryOfResidence).to.equals(250);
    });

    it("Should deny access to new customer from Belarus", async function () {
      const { subscriptions, subscriber, addresses } = await loadFixture(deploySubscriptionFixture);
      expect(await subscriptions.customerExists(addresses.subscriber)).to.false;
      await expect(subscriptions.connect(subscriber)["register(uint16)"](112)).to.be.revertedWith("Country is denied by our policy")
      expect(await subscriptions.customerExists(addresses.subscriber)).to.false;
    });

    it("Should deny access to customer from Belarus even if there are already registered", async function () {
      const { subscriptions, subscriber, addresses, billingContract, billing, minter, nft, cautionPrice, usdtToken, money } = await loadFixture(deploySubscriptionFixture);
      await (await billingContract.connect(billing).setDeniedCountry(112, false)).wait();
      expect(await subscriptions.customerExists(addresses.subscriber)).to.false;

      await (await subscriptions.connect(subscriber)["register(uint16)"](112)).wait();
      expect(await subscriptions.customerExists(addresses.subscriber)).to.true;
      await (await billingContract.connect(billing).setDeniedCountry(112, true)).wait();

      await subscriptions.connect(minter).addValidationSlots(1);
      await nft.connect(minter)["preMint(uint256,string)"](1234, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      await usdtToken.connect(money).transfer(subscriber.address, 120 * 1000000);
      await usdtToken.connect(subscriber).approve(subscriptions.address, 120 * 1000000);
      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 1234, "USDT", 1, true)).to.be.revertedWith("Your country of residence is denied by our policy");

    });

    it("Should deny reload to customer from Belarus even if there are already registered and have subscribed", async function () {
      const { subscriptions, subscriber, addresses, billingContract, billing, minter, nft, cautionPrice, usdtToken, money } = await loadFixture(deploySubscriptionFixture);
      await (await billingContract.connect(billing).setDeniedCountry(112, false)).wait();
      expect(await subscriptions.customerExists(addresses.subscriber)).to.false;

      await (await subscriptions.connect(subscriber)["register(uint16)"](112)).wait();
      expect(await subscriptions.customerExists(addresses.subscriber)).to.true;

      await subscriptions.connect(minter).addValidationSlots(1);
      await nft.connect(minter)["preMint(uint256,string)"](1234, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      await usdtToken.connect(money).transfer(subscriber.address, 120 * 1000000);
      await usdtToken.connect(subscriber).approve(subscriptions.address, 120 * 1000000);
      await (await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 1234, "USDT", 1, true)).wait();

      await (await billingContract.connect(billing).setDeniedCountry(112, true)).wait();

      await expect(subscriptions.connect(subscriber).reloadSubscription(1234, 1)).to.be.revertedWith(
        "Your country of residence is denied by our policy"
      );
    });

    it("Should deny access to customer if accepted GTC are not up to date", async function () {
      const { subscriptions, subscriber, addresses, billingContract, billing, minter, nft, cautionPrice, usdtToken, money, admin } = await loadFixture(deploySubscriptionFixture);
      await (await subscriptions.connect(subscriber)["register(uint16)"](250)).wait();
      expect(await subscriptions.customerExists(addresses.subscriber)).to.true;


      await (await subscriptions.connect(admin).setGtcVersion(1)).wait();

      await subscriptions.connect(minter).addValidationSlots(1);
      await nft.connect(minter)["preMint(uint256,string)"](1234, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      await usdtToken.connect(money).transfer(subscriber.address, 120 * 1000000);
      await usdtToken.connect(subscriber).approve(subscriptions.address, 120 * 1000000);
      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 1234, "USDT", 1, true)).to.be.revertedWith("You must accept our new GTC");
    });

    it("Should not register new customer with bad country code", async function () {
      const { subscriptions, subscriber, addresses } = await loadFixture(deploySubscriptionFixture);
      expect(await subscriptions.customerExists(addresses.subscriber)).to.false;

      await expect(subscriptions.connect(subscriber)["register(uint16)"](8000)).to.be.revertedWith("Unsupported country code");
    });

    it("Should not authorize an user to burn an active nodeid", async function () {
      const { nft, admin, minter, burner, otherAccount, addresses, cautionPrice, subscriptions, usdtToken, money } = await loadFixture(deploySubscriptionFixture);

      await subscriptions.connect(minter).addValidationSlots(1);
      await nft.connect(minter)["preMint(uint256,string)"](1234, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await subscriptions.connect(otherAccount)["register(uint16)"](250);
      await nft.connect(otherAccount).deposit({ value: cautionPrice });
      await nft.connect(otherAccount)["mint()"]();

      await usdtToken.connect(money).transfer(addresses.otherAccount, 120 * 1000000);
      await usdtToken.connect(otherAccount).approve(subscriptions.address, 120 * 1000000);
      await subscriptions.connect(otherAccount)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 1234, "USDT", 1, true);

      await expect(nft.connect(otherAccount).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: Can't burn an active NodeID"
      );

      await expect(nft.connect(burner).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: Can't burn an active NodeID"
      );

      await subscriptions.connect(otherAccount)["stopSubscription(uint256)"](1234);

      await nft.connect(otherAccount).burn(1234);
      await expect(nft.ownerOf(1234)).to.be.revertedWith("ERC721: invalid token ID");
    });

    it("Should allow administrator to stop a subscription and burn the nodeid", async function () {
      const { nft, admin, minter, burner, otherAccount, addresses, cautionPrice, subscriptions, usdtToken, money } = await loadFixture(deploySubscriptionFixture);

      await subscriptions.connect(minter).addValidationSlots(1);
      await nft.connect(minter)["preMint(uint256,string)"](1234, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await subscriptions.connect(otherAccount)["register(uint16)"](250);
      await nft.connect(otherAccount).deposit({ value: cautionPrice });
      await nft.connect(otherAccount)["mint()"]();

      await usdtToken.connect(money).transfer(addresses.otherAccount, 120 * 1000000);
      await usdtToken.connect(otherAccount).approve(subscriptions.address, 120 * 1000000);
      await subscriptions.connect(otherAccount)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 1234, "USDT", 1, true);

      await expect(nft.connect(otherAccount).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: Can't burn an active NodeID"
      );

      await expect(nft.connect(burner).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: Can't burn an active NodeID"
      );

      await subscriptions.connect(admin)["stopSubscription(uint256,address)"](1234, otherAccount.address);

      await nft.connect(burner).burn(1234);
      await expect(nft.ownerOf(1234)).to.be.revertedWith("ERC721: invalid token ID");
    });

    it("Should not authorize an user to burn a nodeid if there is a pending refund", async function () {
      const { nft, admin, minter, burner, otherAccount, addresses, cautionPrice, subscriptions, usdtToken, money } = await loadFixture(deploySubscriptionFixture);

      await subscriptions.connect(minter).addValidationSlots(1);
      await nft.connect(minter)["preMint(uint256,string)"](1234, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await subscriptions.connect(otherAccount)["register(uint16)"](250);
      await nft.connect(otherAccount).deposit({ value: cautionPrice });
      await nft.connect(otherAccount)["mint()"]();

      await usdtToken.connect(money).transfer(addresses.otherAccount, 120 * 1000000);
      await usdtToken.connect(otherAccount).approve(subscriptions.address, 120 * 1000000);
      await subscriptions.connect(otherAccount)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 1234, "USDT", 2, true);

      await expect(nft.connect(otherAccount).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: Can't burn an active NodeID"
      );

      await expect(nft.connect(burner).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: Can't burn an active NodeID"
      );

      await subscriptions.connect(otherAccount)["stopSubscription(uint256)"](1234);

      await expect(nft.connect(otherAccount).burn(1234)).to.be.revertedWith(
        "NodeIdNFT: there is a pending refund, can't burn the token"
      );
    });
  });

  describe("Subscription", function () {
    it("Should reject unregistered customer", async function () {
      const { subscriptions, subscriber } = await loadFixture(deploySubscriptionFixture);

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
        "not a known customer"
      );
    });

    it("Should reject invalid nb periods", async function () {
      const { subscriptions, subscriber } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 0, true)).to.be.revertedWith(
        "nb of prepaid periods must be at least 1"
      );
    });

    it("Should reject subscription if customer is not owner of nodeId", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
        "ERC721: invalid token ID"
      );

      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
        "not owner of nodeId"
      );
    });

    it("Should reject subscription because user hasn't waive its 14 days withdraw right", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();
      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, false)).to.be.revertedWith(
        "You must waive your 14 days withdrawal right"
      );
    });

    it("Should reject subscription for invalid currency", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();
      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "XX", 1, true)).to.be.revertedWith(
        "not a supported currency"
      );
    });

    it("Should reject subscription for invalid amount", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, admin } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();
      await subscriptions.connect(minter).addValidationSlots(1);
      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
        "not enough allowance"
      );
      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDC", 1, true)).to.be.revertedWith(
        "not enough allowance"
      );
    });

    it("Should reject subscription missing validation slots", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, billingContract, usdtToken, money } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      const _amount = await billingContract.usdPriceExcludingTax(0, 1);
      const taxAmount = await billingContract.taxAmount(_amount, 250);
      const total = await billingContract.usdToCurrency((+_amount + +taxAmount), "USDT");

      await usdtToken.connect(money).transfer(addresses.subscriber, total);
      await usdtToken.connect(subscriber).approve(subscriptions.address, total);

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
        "no validation slot available, come back later"
      );
    });

    it("Should reject subscription because its more than 365 days long", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, billingContract, usdtToken, money } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      const _amount = await billingContract.usdPriceExcludingTax(0, 60);
      const taxAmount = await billingContract.taxAmount(_amount, 250);
      const total = await billingContract.usdToCurrency((+_amount + +taxAmount), "USDT");

      await usdtToken.connect(money).transfer(addresses.subscriber, total);
      await usdtToken.connect(subscriber).approve(subscriptions.address, total);
      await subscriptions.connect(minter).addValidationSlots(1);

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 60, true)).to.be.revertedWith(
        "subscriptions over 365 days are not allowed"
      );
    });

    it("Should refund user", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken, admin } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(subscriber)["register(uint16)"](250);

      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      expect(await nft.getDepositBalanceOf(subscriber.address)).to.be.equals(cautionPrice);
      expect(await nft.getFreeDepositOf(subscriber.address)).to.be.equals(1);

      await nft.connect(subscriber)["mint()"]();
      expect(await nft.getDepositBalanceOf(subscriber.address)).to.be.equals(cautionPrice);
      expect(await nft.getFreeDepositOf(subscriber.address)).to.be.equals(0);

      await usdtToken.connect(money).transfer(addresses.subscriber, 120 * 1000000);
      expect(await usdtToken.balanceOf(addresses.subscriber)).to.equals(120 * 1000000);
      await usdtToken.connect(subscriber).approve(subscriptions.address, 120 * 1000000);
      await subscriptions.connect(minter).addValidationSlots(1);

      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);

      expect(await nft.getDepositBalanceOf(subscriber.address)).to.be.equals(0);
      expect(await nft.getFreeDepositOf(subscriber.address)).to.be.equals(0);
    });

    it("Should remove a validation slot at each new subscription", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken, admin } = await loadFixture(deploySubscriptionFixture);

      expect(await subscriptions.validationSlots()).to.be.equals(0);
      await subscriptions.connect(minter).addValidationSlots(1);
      expect(await subscriptions.validationSlots()).to.be.equals(1);

      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();
      await usdtToken.connect(money).transfer(addresses.subscriber, 120 * 1000000);
      await usdtToken.connect(subscriber).approve(subscriptions.address, 120 * 1000000);
      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);

      expect(await subscriptions.validationSlots()).to.be.equals(0);
    });

    it("Should not allow user to reload more than a year", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken, admin, billingContract } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(minter).addValidationSlots(1);
      await subscriptions.connect(subscriber)["register(uint16)"](250);

      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      const amount = await billingContract.usdPriceExcludingTax(0, 1);
      const taxAmount = await billingContract.taxAmount(amount, 250);
      const total = await billingContract.usdToCurrency((+amount + +taxAmount), "USDT");

      await usdtToken.connect(money).transfer(addresses.subscriber, total * 55);
      expect(await usdtToken.balanceOf(addresses.subscriber)).to.equals(total * 55);
      await usdtToken.connect(subscriber).approve(subscriptions.address, total * 55);

      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);

      await expect(subscriptions.connect(subscriber).reloadSubscription(123, 54)).to.be.revertedWith(
        "a weekly subscription reload can't exceed one year"
      );
    });

    it("Should allow user to reload a active subscription", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken, admin, billingContract } = await loadFixture(deploySubscriptionFixture);
      await subscriptions.connect(minter).addValidationSlots(1);
      await subscriptions.connect(subscriber)["register(uint16)"](250);

      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      const amount = await billingContract.usdPriceExcludingTax(0, 1);
      const taxAmount = await billingContract.taxAmount(amount, 250);
      const priceRate = await billingContract.priceRates(0);
      const vatRate = await billingContract.vatRates(250);
      const total = await billingContract.usdToCurrency((+amount + +taxAmount), "USDT");

      await usdtToken.connect(money).transfer(addresses.subscriber, total * 2);
      expect(await usdtToken.balanceOf(addresses.subscriber)).to.equals(total * 2);
      await usdtToken.connect(subscriber).approve(subscriptions.address, total * 2);

      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);

      await expect(await subscriptions.connect(subscriber).reloadSubscription(123, 1))
        .to.emit(subscriptions, "ReloadSubscription")
        .withArgs(addresses.subscriber, 123, 0, 1, "USDT", anyValue, anyValue, amount, taxAmount, priceRate, vatRate);

      const {
        startTs,
        endTs,
        period: storedPeriod,
        currencySymbol,
        _priceRate,
        _vatRate,
      } = await subscriptions.subscriptions(addresses.subscriber, 123);
      expect(startTs).to.greaterThan(0);
      expect(storedPeriod).to.be.equals(0);
      expect(currencySymbol).to.be.equals("USDT");
      expect(endTs.toNumber() - startTs.toNumber()).to.equals(14 * 24 * 60 * 60);
    });

    const SUBSCRIPTION_TESTS = [
      {
        country: 250,
        period: "daily",
        prepaid: 1,
        expectedDuration: 1, // days
        amountExclTaxes: 230,
        taxAmount: 46,
      },
      {
        country: 250,
        period: "daily",
        prepaid: 5,
        expectedDuration: 5,
        amountExclTaxes: 1150,
        taxAmount: 230,
      },
      {
        country: 250,
        period: "weekly",
        prepaid: 1,
        expectedDuration: 7, // days
        amountExclTaxes: 1500,
        taxAmount: 300,
      },
      {
        country: 250,
        period: "weekly",
        prepaid: 5,
        expectedDuration: 35,
        amountExclTaxes: 7500,
        taxAmount: 1500,
      },
      {
        country: 250,
        period: "monthly",
        prepaid: 1,
        expectedDuration: 30,
        amountExclTaxes: 4000,
        taxAmount: 800,
      },
      {
        country: 250,
        period: "monthly",
        prepaid: 10,
        expectedDuration: 300,
        amountExclTaxes: 40000,
        taxAmount: 8000,
      },
      {
        country: 250,
        period: "yearly",
        prepaid: 1,
        expectedDuration: 365,
        amountExclTaxes: 40000,
        taxAmount: 8000,
      },
      {
        country: 442,  // LU
        period: "daily",
        prepaid: 1,
        expectedDuration: 1,
        amountExclTaxes: 230,
        taxAmount: 37,
      },
      {
        country: 442,
        period: "daily",
        prepaid: 5,
        expectedDuration: 5,
        amountExclTaxes: 1150,
        taxAmount: 184,
      },
      {
        country: 442,  // LU
        period: "weekly",
        prepaid: 1,
        expectedDuration: 7,
        amountExclTaxes: 1500,
        taxAmount: 240,
      },
      {
        country: 442,
        period: "weekly",
        prepaid: 5,
        expectedDuration: 35,
        amountExclTaxes: 7500,
        taxAmount: 1200,
      },
      {
        country: 442,
        period: "monthly",
        prepaid: 1,
        expectedDuration: 30,
        amountExclTaxes: 4000,
        taxAmount: 640,
      },
      {
        country: 442,
        period: "monthly",
        prepaid: 10,
        expectedDuration: 300,
        amountExclTaxes: 40000,
        taxAmount: 6400,
      },
      {
        country: 442,
        period: "yearly",
        prepaid: 1,
        expectedDuration: 365,
        amountExclTaxes: 40000,
        taxAmount: 6400,
      },
    ];

    SUBSCRIPTION_TESTS.forEach(({ country, period, prepaid, expectedDuration, amountExclTaxes, taxAmount }) => {
      const periodType = period === "weekly" ? 0 : period === "monthly" ? 1 : period === "yearly" ? 2 : 3;

      ["USDT", "USDC"].forEach((currency) => {
        it(`Should accept new ${period} subscription for ${prepaid} period with ${currency} & ${country}`, async function () {
          const { subscriptions, subscriber, nft, minter, addresses, usdtToken, usdcToken, money, cautionPrice, billingContract, admin } = await loadFixture(
            deploySubscriptionFixture
          );
          await subscriptions.connect(subscriber)["register(uint16)"](country);
          await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
          await nft.connect(subscriber).deposit({ value: cautionPrice });
          await nft.connect(subscriber)["mint()"]();

          const token = currency === "USDT" ? usdtToken : usdcToken;

          const priceRate = await billingContract.priceRates(periodType);
          const vatRate = await billingContract.vatRates(country);
          const total = await billingContract.usdToCurrency((+amountExclTaxes + +taxAmount), currency);

          await token.connect(money).transfer(addresses.subscriber, total);
          expect(await token.balanceOf(addresses.subscriber)).to.equals(total);

          await token.connect(subscriber).approve(subscriptions.address, total);

          expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.false;
          expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.false;
          await subscriptions.connect(minter).addValidationSlots(1);
          await expect(await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](periodType, 123, currency, prepaid, true))
            .to.emit(subscriptions, "NewSubscription")
            .withArgs(addresses.subscriber, 123, periodType, prepaid, currency, anyValue, anyValue, amountExclTaxes, taxAmount, priceRate, vatRate);
          expect(await token.balanceOf(addresses.subscriber)).to.equals(0);
          expect(await token.balanceOf(addresses.destination)).to.equals(total);
          expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;
          expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.true;

          const {
            startTs,
            endTs,
            period: storedPeriod,
            currencySymbol,
          } = await subscriptions.subscriptions(addresses.subscriber, 123);
          expect(startTs).to.greaterThan(0);
          expect(storedPeriod).to.be.equals(periodType);
          expect(currencySymbol).to.be.equals(currency);

          expect(endTs.toNumber() - startTs.toNumber()).to.equals(expectedDuration * 24 * 60 * 60);
        });
      });
    });


    it("Should reject if customer has already an active subscription for this node id", async function () {
      const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice, admin } = await loadFixture(
        deploySubscriptionFixture
      );
      await subscriptions.connect(minter).addValidationSlots(2);
      await subscriptions.connect(subscriber)["register(uint16)"](250);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
      await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

      expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.false;
      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
      expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

      await expect(subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
        "already has an active subscription for this node id"
      );

      const { endTs: originalEndTs } = await subscriptions.subscriptions(addresses.subscriber, 123);

      // Move 2 days later
      await ethers.provider.send("evm_increaseTime", [2 * 24 * 60 * 60]);
      await ethers.provider.send("evm_mine", []);
      expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

      // Move 5 days later
      await ethers.provider.send("evm_increaseTime", [5 * 24 * 60 * 60]);
      await ethers.provider.send("evm_mine", []);
      expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.false;

      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
      const { endTs: newEndTs } = await subscriptions.subscriptions(addresses.subscriber, 123);

      expect(newEndTs).to.greaterThan(originalEndTs);
    });
  });

  it("Should allow a customer to have 2 active subscription for different node ids", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(2);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(minter)["preMint(uint256,string)"](456, "HvUBEqBfzHJPKbktYKD5y438yTYMroMrw");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 456, "USDT", 1, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 456)).to.true;

    // Move 7 days later
    await ethers.provider.send("evm_increaseTime", [7 * 24 * 60 * 60]);
    await ethers.provider.send("evm_mine", []);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.false;
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 456)).to.false;
  });

  it("Should refuse to stop a non existing subscription", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await expect(subscriptions.connect(subscriber)["stopSubscription(uint256)"](123)).to.be.revertedWith(
      "no active subscription for this node id"
    );
  });

  it("Should refuse to stop an expired subscription", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    // Move 7 days later
    await ethers.provider.send("evm_increaseTime", [7 * 24 * 60 * 60]);
    await ethers.provider.send("evm_mine", []);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.false;

    await expect(subscriptions.connect(subscriber)["stopSubscription(uint256)"](123)).to.be.revertedWith(
      "no active subscription for this node id"
    );
  });

  it("Only node id owner can stop an active subscription", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, admin, otherAccount, cautionPrice } =
      await loadFixture(deploySubscriptionFixture);

    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await subscriptions.connect(admin)["register(uint16)"](250);
    await subscriptions.connect(otherAccount)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    await expect(subscriptions.connect(otherAccount)["stopSubscription(uint256)"](123)).to.be.revertedWith(
      "no active subscription for this node id"
    );
    await expect(subscriptions.connect(admin)["stopSubscription(uint256)"](123)).to.be.revertedWith(
      "no active subscription for this node id"
    );
  });

  it("Should stop an active subscription", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;
    expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.true;

    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);

    expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.false;

    // Get timestamp of last mined block
    const { timestamp: lastBlockTS } = await ethers.provider.getBlock(ethers.provider.getBlockNumber());
    const { endTs } = await subscriptions.subscriptions(subscriber.address, 123);
    expect(endTs).to.lessThanOrEqual(lastBlockTS);

    const { currencySymbol, pending } = await subscriptions.refunds(subscriber.address, 123);
    expect(currencySymbol).to.equals("USDT");
    expect(pending).to.equals(true);
  });

  const REFUNDS_TESTS = [
    {
      country: 250,
      period: "daily",
      prepaid: 1,
      refundAfterDays: 0,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "daily",
      prepaid: 6,
      refundAfterDays: 5,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "daily",
      prepaid: 7,
      refundAfterDays: 5,  // one day to refund because the test adds 5 days and a block, meaning that we are 6th day subscription
      expectedRefund: 2.76, // one day => 1.42 ex.VAT / total: 1.70
    },
    {
      country: 250,
      period: "daily", // at subscription, 42.60 $ ex.VAT + 8.52 VAT / total: 51.12 
      prepaid: 30,
      refundAfterDays: 12,
      expectedRefund: 46.92, // 17 days => 24.14 ex.VAT / total: 28.97  
    },
    {
      country: 250,
      period: "daily",
      prepaid: 365,
      refundAfterDays: 30,
      expectedRefund: 921.84, // 334 days => 474.28 ex.VAT + 94.86 VAT / total: 569.14
    },
    {
      country: 250,
      period: "daily",
      prepaid: 365,
      refundAfterDays: 364,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 1,
      refundAfterDays: 0,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 1,
      refundAfterDays: 5,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 2,
      refundAfterDays: 5,
      expectedRefund: 18, // one week => 15 HT / 18 TTC
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 3,
      refundAfterDays: 5,
      expectedRefund: 36, // two weeks
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 2,
      refundAfterDays: 10,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 3,
      refundAfterDays: 10,
      expectedRefund: 18, // one week
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 3,
      refundAfterDays: 6,
      expectedRefund: 36, // two week
    },
    {
      country: 250,
      period: "weekly",
      prepaid: 3,
      refundAfterDays: 7,
      expectedRefund: 18, // one week
    },
    {
      country: 250,
      period: "monthly",
      prepaid: 1,
      refundAfterDays: 0,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "monthly",
      prepaid: 1,
      refundAfterDays: 10,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "monthly",
      prepaid: 2,
      refundAfterDays: 10,
      expectedRefund: 48,
    },
    {
      country: 442,
      period: "monthly",
      prepaid: 2,
      refundAfterDays: 10,
      expectedRefund: 46.4,
    },
    {
      country: 250,
      period: "monthly",
      prepaid: 2,
      refundAfterDays: 29,
      expectedRefund: 48,
    },
    {
      country: 250,
      period: "monthly",
      prepaid: 2,
      refundAfterDays: 35,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "monthly",
      prepaid: 10,
      refundAfterDays: 50,
      expectedRefund: 384,
    },
    {
      country: 250,
      period: "yearly",
      prepaid: 1,
      refundAfterDays: 0,
      expectedRefund: 0,
    },
    {
      country: 250,
      period: "yearly",
      prepaid: 1,
      refundAfterDays: 200,
      expectedRefund: 0,
    },
  ];

  REFUNDS_TESTS.forEach(({ country, period, prepaid, refundAfterDays, expectedRefund }) => {
    it(`Should compute refund amount for ${prepaid} ${period} period(s), country ${country} and stop after ${refundAfterDays} day(s)`, async function () {
      const periodType = period === "weekly" ? 0 : period === "monthly" ? 1 : period === "yearly" ? 2 : 3;
      const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice, billingContract, admin } = await loadFixture(
        deploySubscriptionFixture
      );
      await subscriptions.connect(minter).addValidationSlots(1);
      await subscriptions.connect(subscriber)["register(uint16)"](country);
      await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
      await nft.connect(subscriber).deposit({ value: cautionPrice });
      await nft.connect(subscriber)["mint()"]();

      const _amount = await billingContract.usdPriceExcludingTax(periodType, prepaid);
      const taxAmount = await billingContract.taxAmount(_amount, 250);
      const total = await billingContract.usdToCurrency((+_amount + +taxAmount), "USDT");

      await usdtToken.connect(money).transfer(addresses.subscriber, total);
      await usdtToken.connect(subscriber).approve(subscriptions.address, total);

      await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](periodType, 123, "USDT", prepaid, true);
      expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

      await ethers.provider.send("evm_increaseTime", [refundAfterDays * 24 * 60 * 60]);
      await ethers.provider.send("evm_mine", []);
      expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

      await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);

      const { amount, currencySymbol } = await subscriptions.refunds(subscriber.address, 123);
      expect(amount).to.equals(
        expectedRefund * 100,
        `Invalid refund amount for ${prepaid} ${period} period(s), country ${country} & stop after ${refundAfterDays} day(s)`
      );
      if (amount.gt(0)) expect(currencySymbol).to.equals("USDT");
    });
  });

  it("Should complete a pending refund", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, refunder, cautionPrice, billingContract, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    const _amount = await billingContract.usdPriceExcludingTax(0, 2);
    const taxAmount = await billingContract.taxAmount(_amount, 250);
    const total = await billingContract.usdToCurrency(_amount + taxAmount, "USDT");

    await usdtToken.connect(money).transfer(addresses.subscriber, total);
    await usdtToken.connect(money).transfer(addresses.refunder, total);
    await usdtToken.connect(subscriber).approve(subscriptions.address, total);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;

    const { currencySymbol, pending, amount } = await subscriptions.refunds(subscriber.address, 123);
    expect(currencySymbol).to.equals("USDT");
    expect(pending).to.equals(true);
    expect(amount).to.equals(1800);

    const __amount = await billingContract.usdToCurrency(amount, currencySymbol);

    const usdtBalanceBeforeRefund = await usdtToken.balanceOf(addresses.subscriber);
    await usdtToken.connect(refunder).approve(subscriptions.address, __amount);
    await subscriptions.connect(refunder).completeRefund(subscriber.address, 123);

    const { pending: pendingAfterComplete } = await subscriptions.refunds(subscriber.address, 123);
    expect(pendingAfterComplete).to.equals(false);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.false;

    const usdtBalanceAfterRefund = await usdtToken.balanceOf(addresses.subscriber);
    expect(usdtBalanceAfterRefund).to.equals(usdtBalanceBeforeRefund.add(__amount));
  });

  it("Only refunder can refund", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, cautionPrice, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    await subscriptions.refunds(subscriber.address, 123);
    await usdtToken.connect(money).approve(subscriptions.address, 12000);
    await expect(subscriptions.connect(money).completeRefund(subscriber.address, 123)).to.be.revertedWith(
      "not a refunder"
    );
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;
  });

  it("Should revert if completing a non existing refund", async function () {
    const { subscriptions, subscriber, money, refunder } = await loadFixture(deploySubscriptionFixture);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await expect(subscriptions.connect(refunder).completeRefund(subscriber.address, 123)).to.be.revertedWith(
      "no pending refund for this wallet & node id"
    );
  });

  it("Should revert if allowance is not enough", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, refunder, cautionPrice, billingContract, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    const _amount = await billingContract.usdPriceExcludingTax(0, 2);
    const taxAmount = await billingContract.taxAmount(_amount, 250);
    const total = await billingContract.usdToCurrency((+_amount + +taxAmount), "USDT");

    await usdtToken.connect(money).transfer(addresses.subscriber, total);
    await usdtToken.connect(money).transfer(addresses.refunder, total);
    await usdtToken.connect(subscriber).approve(subscriptions.address, total);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;

    const { currencySymbol, pending, amount } = await subscriptions.refunds(subscriber.address, 123);
    expect(currencySymbol).to.equals("USDT");
    expect(pending).to.equals(true);
    expect(amount).to.equals(1800);

    await usdtToken.connect(refunder).approve(subscriptions.address, 1700);
    await expect(subscriptions.connect(refunder).completeRefund(subscriber.address, 123)).to.be.revertedWith(
      "not enough allowance"
    );
  });

  it("Should revert if complete twice", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, refunder, cautionPrice, billingContract, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(1);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    const _amount = await billingContract.usdPriceExcludingTax(0, 2);
    const taxAmount = await billingContract.taxAmount(_amount, 250);
    const total = await billingContract.usdToCurrency((+_amount + +taxAmount), "USDT");

    await usdtToken.connect(money).transfer(addresses.subscriber, total);
    await usdtToken.connect(money).transfer(addresses.refunder, total);
    await usdtToken.connect(subscriber).approve(subscriptions.address, total);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;

    const { currencySymbol, pending, amount } = await subscriptions.refunds(subscriber.address, 123);
    expect(currencySymbol).to.equals("USDT");
    expect(pending).to.equals(true);
    expect(amount).to.equals(1800);

    const __amount = await billingContract.usdToCurrency(amount, currencySymbol);

    await usdtToken.connect(refunder).approve(subscriptions.address, __amount);
    await subscriptions.connect(refunder).completeRefund(subscriber.address, 123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.false;

    await expect(subscriptions.connect(refunder).completeRefund(subscriber.address, 123)).to.be.revertedWith(
      "no pending refund for this wallet & node id"
    );
  });

  it("Should allow two consecutive subscription / refund", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, refunder, cautionPrice, billingContract, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(2);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    const _amount = await billingContract.usdPriceExcludingTax(0, 2);
    const taxAmount = await billingContract.taxAmount(_amount, 250);
    const total = await billingContract.usdToCurrency((+_amount + +taxAmount) * 2, "USDT"); // x2 because of two consecutive subscription

    await usdtToken.connect(money).transfer(addresses.subscriber, total);
    await usdtToken.connect(money).transfer(addresses.refunder, total);
    await usdtToken.connect(subscriber).approve(subscriptions.address, total);

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;

    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;

    const { currencySymbol, pending, amount } = await subscriptions.refunds(subscriber.address, 123);
    expect(currencySymbol).to.equals("USDT");
    expect(pending).to.equals(true);
    expect(amount).to.equals(1800);

    const __amount = await billingContract.usdToCurrency(amount, currencySymbol);

    await usdtToken.connect(refunder).approve(subscriptions.address, __amount);
    await subscriptions.connect(refunder).completeRefund(subscriber.address, 123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.false;

    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;
    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;
    await usdtToken.connect(refunder).approve(subscriptions.address, __amount);
    await subscriptions.connect(refunder).completeRefund(subscriber.address, 123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.false;
  });

  it("Should reject if another customer has already an active subscription for this node id", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, usdtToken, money, otherAccount, cautionPrice, admin } = await loadFixture(
      deploySubscriptionFixture
    );
    await subscriptions.connect(minter).addValidationSlots(2);
    await subscriptions.connect(subscriber)["register(uint16)"](250);
    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 500000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 500000000);

    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.false;
    expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.false;
    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;
    expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.true;

    await subscriptions.connect(otherAccount)["register(uint16)"](250);

    await usdtToken.connect(money).transfer(addresses.otherAccount, 500000000);
    await usdtToken.connect(otherAccount).approve(subscriptions.address, 500000000);

    // subscriber transfers nodeid to otherAccount
    await nft.connect(subscriber).transferFrom(addresses.subscriber, addresses.otherAccount, 123);
    expect(await nft.ownerOf(123)).to.equals(addresses.otherAccount);

    expect(await subscriptions.hasActiveSubscription(addresses.otherAccount, 123)).to.false;
    await expect(subscriptions.connect(otherAccount)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true)).to.be.revertedWith(
      "nodeid already has a subscription"
    );

    // Move 7 days later
    await ethers.provider.send("evm_increaseTime", [7 * 24 * 60 * 60]);
    await ethers.provider.send("evm_mine", []);
    expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.false;

    await subscriptions.connect(otherAccount)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);
    expect(await subscriptions.hasActiveSubscription(addresses.otherAccount, 123)).to.true;
  });

  it("Should be able to refund even if a new subscription has been started", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken, admin } =
      await loadFixture(deploySubscriptionFixture);
    await subscriptions.connect(subscriber)["register(uint16)"](250);

    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 120 * 1000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 120 * 1000000);
    await subscriptions.connect(minter).addValidationSlots(1);

    // Subscribe
    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 2, true);

    expect(await nft.getDepositBalanceOf(subscriber.address)).to.be.equals(0);
    expect(await nft.getFreeDepositOf(subscriber.address)).to.be.equals(0);

    // Stop
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.false;
    await subscriptions.connect(subscriber)["stopSubscription(uint256)"](123);
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;

    // Second subscription
    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](0, 123, "USDT", 1, true);

    // Check that first pending refund is still pending
    expect(await subscriptions.hasPendingRefund(addresses.subscriber, 123)).to.true;

    // User can't stop subscription if a pending refund is still pending
    await expect(subscriptions.connect(subscriber)["stopSubscription(uint256)"](123)).to.be.revertedWith(
      "a pending refund exists for this node id, can't stop this subscription"
    );
  });

  it("Should be able to reload a yearly subscription", async function () {
    const { subscriptions, subscriber, nft, minter, addresses, cautionPrice, money, usdtToken, admin } =
      await loadFixture(deploySubscriptionFixture);
    await subscriptions.connect(subscriber)["register(uint16)"](250);

    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    await nft.connect(subscriber).deposit({ value: cautionPrice });
    await nft.connect(subscriber)["mint()"]();

    await usdtToken.connect(money).transfer(addresses.subscriber, 3000 * 1000000);
    await usdtToken.connect(subscriber).approve(subscriptions.address, 3000 * 1000000);
    await subscriptions.connect(minter).addValidationSlots(1);

    // Subscribe
    await subscriptions.connect(subscriber)["newSubscription(uint8,uint256,string,uint16,bool)"](2, 123, "USDT", 1, true);

    // Reload
    await expect(subscriptions.connect(subscriber).reloadSubscription(123, 1)).to.be.revertedWith(
      "a yearly subscription can only be reloaded the last month and only for one more year"
    );

    // advance time by 335 days and mine a new block
    await time.increase(28944000);

    // Reload more than one year
    await expect(subscriptions.connect(subscriber).reloadSubscription(123, 2)).to.be.revertedWith(
      "a yearly subscription can only be reloaded the last month and only for one more year"
    );

    // Reload one year
    await subscriptions.connect(subscriber).reloadSubscription(123, 1);
  });
});
