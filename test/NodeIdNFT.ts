import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

const USDT_TOKEN = "0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7";
const USDC_TOKEN = "0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E";

describe("NodeIdNFT", function () {
  async function deployNFTFixture() {
    const [deployer, admin, billing, burner, destinationWallet, minter, proxyAdmin, refunder, subscriber, destination, otherAccount, money] =
      await ethers.getSigners();
    const addresses = {
      deployer: await deployer.getAddress(),
      admin: await admin.getAddress(),
      billing: await billing.getAddress(),
      minter: await minter.getAddress(),
      burner: await burner.getAddress(),
      otherAccount: await otherAccount.getAddress(),
      destination: await destination.getAddress(),
      refunder: await refunder.getAddress(),
    };

    const AccessControl = await ethers.getContractFactory("AccessControl");
    const access = await AccessControl.deploy();

    const roles = {
      ADMIN: await access.ADMIN_ROLE(),
      BURNER: await access.BURNER_ROLE(),
      MINTER: await access.MINTER_ROLE(),
      BILLING: await access.BILLING_ROLE(),
      REFUNDER: await access.REFUNDER_ROLE(),
    };

    await access.initialize();
    await access.grantRole(roles.ADMIN, addresses.admin);
    await access.renounceRole(roles.ADMIN, addresses.deployer);
    await access.connect(admin).grantRole(roles.BILLING, addresses.billing);
    await access.connect(admin).grantRole(roles.MINTER, addresses.minter);
    await access.connect(admin).grantRole(roles.BURNER, addresses.burner);
    await access.connect(admin).grantRole(roles.REFUNDER, addresses.refunder);

    const Billing = await ethers.getContractFactory("Billing");
    const billingContract = await Billing.deploy();
    await billingContract.initialize(access.address, USDT_TOKEN, USDC_TOKEN);

    const NFT = await ethers.getContractFactory("NodeIdNFT");
    const nft = await NFT.deploy();

    await nft.initialize(access.address);
    const cautionPrice = await nft.cautionPrice(); // get default caution price

    const Subscriptions = await ethers.getContractFactory("Subscriptions");
    const subscriptions = await Subscriptions.deploy();
    await subscriptions.initialize(access.address, billingContract.address, nft.address, addresses.destination);
    await nft.connect(admin).setSubscriptions(subscriptions.address); // Set caution price

    return { nft, roles, addresses, deployer, admin, billing, minter, burner, otherAccount, cautionPrice, refunder };
  }

  describe("Deployment", function () {
    it("Should set the right name & symbol", async function () {
      const { nft } = await loadFixture(deployNFTFixture);
      expect(await nft.name()).to.equal("NodeIdNFT");
      expect(await nft.symbol()).to.equal("NID");
    });

    it("Should have valid tokenURI", async function () {
      const { nft, minter, addresses } = await loadFixture(deployNFTFixture);
      await nft.connect(minter)["preMint(uint256)"]("1234");
      expect(await nft.tokenURI("1234")).to.be.equals("https://app.nodz.network/1234");
    });
  });

  describe("Access Control", function () {
    it("Only MINTER can mint tokens", async function () {
      const { nft, deployer, admin, minter, burner, addresses } = await loadFixture(deployNFTFixture);
      await expect(nft.ownerOf("5")).to.be.revertedWith("ERC721: invalid token ID");

      await expect(nft.connect(admin)["preMint(uint256)"]("5")).to.be.revertedWith("not a minter");
      await expect(nft.connect(deployer)["preMint(uint256)"]("5")).to.be.revertedWith("not a minter");
      await expect(nft.connect(burner)["preMint(uint256)"]("5")).to.be.revertedWith("not a minter");

      await nft.connect(minter)["preMint(uint256)"]("5");
      expect(await nft.ownerOf("5")).to.be.equals(addresses.minter);
    });

    it("Only BURNER or token owner can burn tokens", async function () {
      const { nft, deployer, admin, minter, burner, otherAccount, addresses, cautionPrice } = await loadFixture(deployNFTFixture);

      await nft.connect(minter)["preMint(uint256)"]("1234");
      await nft.connect(otherAccount).deposit({ value: cautionPrice });
      await nft.connect(otherAccount).mintBlank();

      await nft.connect(minter)["preMint(uint256)"]("5678");
      await nft.connect(otherAccount).deposit({ value: cautionPrice });
      await nft.connect(otherAccount).mintBlank();

      expect(await nft.ownerOf("1234")).to.be.equals(addresses.otherAccount);
      expect(await nft.ownerOf("5678")).to.be.equals(addresses.otherAccount);

      await expect(nft.connect(admin).burn("1234")).to.be.revertedWith(
        "NodeIdNFT: caller is not token owner nor approved nor a burner"
      );
      await expect(nft.connect(deployer).burn("1234")).to.be.revertedWith(
        "NodeIdNFT: caller is not token owner nor approved nor a burner"
      );
      await expect(nft.connect(minter).burn("1234")).to.be.revertedWith(
        "NodeIdNFT: caller is not token owner nor approved nor a burner"
      );

      await expect(await nft.connect(burner).burn("1234")).to.emit(nft, "BurnNFT").withArgs("1234");
      await expect(nft.ownerOf("1234")).to.be.revertedWith("ERC721: invalid token ID");

      await nft.connect(otherAccount).burn("5678");
      await expect(nft.ownerOf("5678")).to.be.revertedWith("ERC721: invalid token ID");
    });

    it("A pre minted nft should throw an event", async function () {
      const { nft, minter } = await loadFixture(deployNFTFixture);

      await expect(nft.connect(minter)["preMint(uint256)"]("1234"))
        .to.emit(nft, "NewPremintedNFT")
        .withArgs("1234", "");
    });

    it("A minted nft should throw an event", async function () {
      const { nft, billing, minter, otherAccount, cautionPrice, addresses } = await loadFixture(deployNFTFixture);

      await nft.connect(minter)["preMint(uint256)"]("1234");
      await nft.connect(otherAccount).deposit({ value: cautionPrice });

      await expect(nft.connect(otherAccount).mintBlank())
        .to.emit(nft, "NewMintedNFT")
        .withArgs("1234", addresses.otherAccount, "");

      await nft.connect(minter)["preMint(uint256,string)"]("5678", "NZv6zL77VMYdBmbYMjzn4iikP3MURsv8F");
      await nft.connect(otherAccount).deposit({ value: cautionPrice });

      await expect(nft.connect(otherAccount)["mint()"]())
        .to.emit(nft, "NewMintedNFT")
        .withArgs("5678", addresses.otherAccount, "NZv6zL77VMYdBmbYMjzn4iikP3MURsv8F");
    });

    it("Only REFUNDER could refund a user", async function () {
      const { nft, billing, minter, otherAccount, cautionPrice, addresses, refunder } = await loadFixture(deployNFTFixture);

      await nft.connect(minter)["preMint(uint256)"]("1234");
      await nft.connect(otherAccount).deposit({ value: cautionPrice });
      expect(await nft.getDepositBalanceOf(otherAccount.address)).to.be.equals(cautionPrice);
      expect(await nft.getClaimableBalanceOf(otherAccount.address)).to.be.equals(0);
      expect(await nft.getFreeDepositOf(otherAccount.address)).to.be.equals(1);

      await nft.connect(otherAccount).mintBlank();
      expect(await nft.getDepositBalanceOf(otherAccount.address)).to.be.equals(cautionPrice);
      expect(await nft.getClaimableBalanceOf(otherAccount.address)).to.be.equals(cautionPrice);
      expect(await nft.getFreeDepositOf(otherAccount.address)).to.be.equals(0);

      await expect(nft.connect(otherAccount).refund(addresses.otherAccount)).to.be.revertedWith("NodeIdNFT: caller is not a refunder user nor subscription contrat");
      await nft.connect(refunder).refund(addresses.otherAccount);
      expect(await nft.getDepositBalanceOf(otherAccount.address)).to.be.equals(0);
      expect(await nft.getClaimableBalanceOf(otherAccount.address)).to.be.equals(0);
      expect(await nft.getFreeDepositOf(otherAccount.address)).to.be.equals(0);
    });
  });

  describe("Token unicity", function () {
    it("Should have a unique TokenId and NodeId", async function () {
      const { nft, minter, addresses } = await loadFixture(deployNFTFixture);

      await nft.connect(minter)["preMint(uint256,string)"]("1234", "NZv6zL77VMYdBmbYMjzn4iikP3MURsv8F");
      await nft.connect(minter)["preMint(uint256,string)"]("5678", "2eraCsE3WijaEnkcFX77hbK494TneHXkY");

      await expect(nft.connect(minter)["preMint(uint256,string)"]("5678", "M17yakji8RzPBFJNPXnksjoeabg7BXUJH")).to.be.revertedWith("ERC721: token already minted");
      await expect(nft.connect(minter)["preMint(uint256,string)"]("9123", "2eraCsE3WijaEnkcFX77hbK494TneHXkY")).to.be.revertedWith("NodeIdNFT: NodeID is already registered");
      await expect(nft.connect(minter)["preMint(uint256,string)"]("0", "M17yakji8RzPBFJNPXnksjoeabg7BXUJH")).to.be.revertedWith("NodeIdNFT: tokendId 0 is reserved");

      await nft.connect(minter)["preMint(uint256)"]("9123");
      await expect(nft.connect(minter).setNodeID("9123", "2eraCsE3WijaEnkcFX77hbK494TneHXkY")).to.be.revertedWith("NodeIdNFT: NodeID is already registered");
      await expect(nft.connect(minter).setNodeID("9123", "M17yakji8RzPBFJNPXnksjoeabg7BXUJH"))
        .to.emit(nft, "NewMintedNFT")
        .withArgs("9123", minter.address, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");

      expect(await nft.nodeIds("1234")).to.be.equals("NZv6zL77VMYdBmbYMjzn4iikP3MURsv8F");
      expect(await nft.nodeIds("5678")).to.be.equals("2eraCsE3WijaEnkcFX77hbK494TneHXkY");
      expect(await nft.nodeIds("9123")).to.be.equals("M17yakji8RzPBFJNPXnksjoeabg7BXUJH");
    });
  });

  describe("Should compute the right b58decode", function () {
    it("Should have a unique TokenId and NodeId", async function () {
      const { nft } = await loadFixture(deployNFTFixture);

      await expect(await nft.base58Decode("81U2wpq95De12KchbFhatf2fBt6KTFmAr")).to.equal("0x4CDF726B32F2E9A39B040611B68a772D176C614B");
      await expect(await nft.base58Decode("M64wor3rGStFTBWuzvkGPYYsWUu2dX7S2")).to.equal("0xDc57e379691271786F29f0e454f847651a331128");
      await expect(await nft.base58Decode("Ck4VEAv4JTMgnijLQDeGCWST8Zdgxe9Q6")).to.equal("0x80ce2e35b99448B9650d060680Ae1baa5AA9885A");
    });
  });
});
