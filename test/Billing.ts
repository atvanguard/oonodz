import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

const USDT_TOKEN = "0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7";
const USDC_TOKEN = "0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E";

describe("Billing", function () {
  async function deployBillingFixture() {
    const [deployer, admin, billing, otherAccount] = await ethers.getSigners();
    const addresses = {
      deployer: await deployer.getAddress(),
      admin: await admin.getAddress(),
      billing: await billing.getAddress(),
      otherAccount: await otherAccount.getAddress(),
    };

    const AccessControl = await ethers.getContractFactory("AccessControl");
    const access = await AccessControl.deploy();

    const roles = {
      ADMIN: await access.ADMIN_ROLE(),
      BURNER: await access.BURNER_ROLE(),
      MINTER: await access.MINTER_ROLE(),
      BILLING: await access.BILLING_ROLE(),
    };

    await access.initialize();
    await access.grantRole(roles.ADMIN, addresses.admin);
    await access.renounceRole(roles.ADMIN, addresses.deployer);
    await access.connect(admin).grantRole(roles.BILLING, addresses.billing);

    const Billing = await ethers.getContractFactory("Billing");
    const billingContract = await Billing.deploy();

    await billingContract.initialize(access.address, USDT_TOKEN, USDC_TOKEN);

    return { billingContract, roles, addresses, deployer, admin, billing, otherAccount };
  }

  describe("Deployment", function () {
    it("Should accept USDT & USDC", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      expect(await billingContract.isSupportedCurrency("USDT")).to.true;
      expect(await billingContract.isSupportedCurrency("USDC")).to.true;
      expect(await billingContract.currencies("USDT")).to.equal(USDT_TOKEN);
      expect(await billingContract.currencies("USDC")).to.equal(USDC_TOKEN);
    });

    it("Should has VAT Rate for FR and LU", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      expect(await billingContract.vatRates(250)).to.equal(20);
      expect(await billingContract.vatRates(442)).to.equal(16);
    });
  });

  describe("Currency", function () {
    it("Should revert for unsupported currency", async function () {
      const { billingContract, billing } = await loadFixture(deployBillingFixture);
      await expect(billingContract.usdToCurrency(10, "BUSD")).to.be.revertedWith("Billing: unsupported currency");
      await billingContract.connect(billing).setCurrency("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39");
    });

    it("Should compute exchange from USD to USDC", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      expect(await billingContract.usdToCurrency(10000, "USDC")).to.equal(100000000);
      expect(await billingContract.usdToCurrency(10099, "USDC")).to.equal(100990000);
      expect(await billingContract.usdToCurrency(10001, "USDC")).to.equal(100010000);
    });

    it("Should compute exchange from USD to USDT", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      expect(await billingContract.usdToCurrency(10000, "USDT")).to.equal(100000000);
      expect(await billingContract.usdToCurrency(10099, "USDT")).to.equal(100990000);
      expect(await billingContract.usdToCurrency(10001, "USDT")).to.equal(100010000);
    });

    it("Shoud allow billing to add new currency", async function () {
      const { billingContract, billing } = await loadFixture(deployBillingFixture);
      expect(await billingContract.isSupportedCurrency("BUSD")).to.false;
      await expect(billingContract.connect(billing).setCurrency("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39"))
        .to.emit(billingContract, "CurrencyChanged")
        .withArgs("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39");
      expect(await billingContract.isSupportedCurrency("BUSD")).to.true;
      expect(await billingContract.currencies("BUSD")).to.equal("0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39");
    });

    it("Shoud allow billing to remove a currency", async function () {
      const { billingContract, billing } = await loadFixture(deployBillingFixture);
      await billingContract.connect(billing).setCurrency("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39");
      expect(await billingContract.isSupportedCurrency("BUSD")).to.true;
      expect(await billingContract.currencies("BUSD")).to.equal("0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39");
      await expect(billingContract.connect(billing).setCurrency("BUSD", "0x0000000000000000000000000000000000000000"))
        .to.emit(billingContract, "CurrencyChanged")
        .withArgs("BUSD", "0x0000000000000000000000000000000000000000");
      expect(await billingContract.isSupportedCurrency("BUSD")).to.false;
    });

    it("Shoud allow ONLY billing to add new currency", async function () {
      const { billingContract, deployer, admin, otherAccount } = await loadFixture(deployBillingFixture);
      await expect(
        billingContract.connect(deployer).setCurrency("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39")
      ).to.be.revertedWith("not a billing");
      await expect(
        billingContract.connect(admin).setCurrency("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39")
      ).to.be.revertedWith("not a billing");
      await expect(
        billingContract.connect(otherAccount).setCurrency("BUSD", "0x9C9e5fD8bbc25984B178FdCE6117Defa39d2db39")
      ).to.be.revertedWith("not a billing");
    });

    it("Shoud allow ONLY billing to remove a currency", async function () {
      const { billingContract, deployer, admin, otherAccount } = await loadFixture(deployBillingFixture);
      await expect(
        billingContract.connect(deployer).setCurrency("USDT", "0x0000000000000000000000000000000000000000")
      ).to.be.revertedWith("not a billing");
      await expect(
        billingContract.connect(admin).setCurrency("USDT", "0x0000000000000000000000000000000000000000")
      ).to.be.revertedWith("not a billing");
      await expect(
        billingContract.connect(otherAccount).setCurrency("USDT", "0x0000000000000000000000000000000000000000")
      ).to.be.revertedWith("not a billing");
    });
  });

  describe("VAT Rates", function () {
    it("Shoud allow billing to add VAT rate", async function () {
      const { billingContract, billing } = await loadFixture(deployBillingFixture);
      expect(await billingContract.vatRates(800)).to.equals(0);
      await expect(billingContract.connect(billing).setVatRate(800, 11))
        .to.emit(billingContract, "VatRateChanged")
        .withArgs(800, 11);
      expect(await billingContract.vatRates(800)).to.equals(11);

      await expect(billingContract.connect(billing).setVatRate(8000, 11)).to.be.revertedWith("Billing: unsupported country code");
    });

    it("Shoud allow ONLY billing to add new currency", async function () {
      const { billingContract, deployer, admin, otherAccount } = await loadFixture(deployBillingFixture);
      await expect(billingContract.connect(deployer).setVatRate(800, 11)).to.be.revertedWith("not a billing");
      await expect(billingContract.connect(admin).setVatRate(800, 11)).to.be.revertedWith("not a billing");
      await expect(billingContract.connect(otherAccount).setVatRate(800, 11)).to.be.revertedWith("not a billing");
    });

    it("Should compute tax amount based on country of residence", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      // FR = 20% of 10000.00 USD = 2000.00 USD
      expect(await billingContract.taxAmount(1000000, 250)).to.equals(200000);
      // LU = 16% of 10000.00 USD = 1600.70 USD
      expect(await billingContract.taxAmount(1000000, 442)).to.equals(160000);
      // XX = 0% of 10000.00 USD = 0 USD
      expect(await billingContract.taxAmount(1000000, 800)).to.equals(0);

      // FR = 20% of 999.00 USD = 199.80 USD
      expect(await billingContract.taxAmount(99900, 250)).to.equals(19980);
      // LU = 16% of 999.00 USD = 159.84 USD
      expect(await billingContract.taxAmount(99900, 442)).to.equals(15984);

      // FR = 20% of 999.99 USD = 199.998 USD = 200.00 USD
      // If the third digit after the decimal point is equal to or greater than 5, round up to the nearest cent
      expect(await billingContract.taxAmount(99999, 250)).to.equals(20000);
      // LU = 16% of 999.99 USD = 159.9984 USD = 160.00 USD
      // If the third digit after the decimal point is equal to or greater than 5, round up to the nearest cent
      expect(await billingContract.taxAmount(99999, 442)).to.equals(16000);
    });
  });

  describe("Prices", function () {
    it("Shoud return default prices", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      // Daily
      expect(await billingContract.usdPriceExcludingTax(3, 1)).to.equal(230);
      expect(await billingContract.usdPriceExcludingTax(3, 2)).to.equal(460);
      expect(await billingContract.usdPriceExcludingTax(3, 10)).to.equal(2300);
      expect(await billingContract.usdPriceExcludingTax(3, 255)).to.equal(58650);
      expect(await billingContract.usdPriceExcludingTax(3, 365)).to.equal(83950);

      // Weekly
      expect(await billingContract.usdPriceExcludingTax(0, 1)).to.equal(1500);
      expect(await billingContract.usdPriceExcludingTax(0, 2)).to.equal(3000);
      expect(await billingContract.usdPriceExcludingTax(0, 10)).to.equal(15000);
      expect(await billingContract.usdPriceExcludingTax(0, 255)).to.equal(382500);

      // Montly
      expect(await billingContract.usdPriceExcludingTax(1, 1)).to.equal(4000);
      expect(await billingContract.usdPriceExcludingTax(1, 2)).to.equal(8000);
      expect(await billingContract.usdPriceExcludingTax(1, 10)).to.equal(40000);
      expect(await billingContract.usdPriceExcludingTax(1, 255)).to.equal(1020000);

      // Yearly
      expect(await billingContract.usdPriceExcludingTax(2, 1)).to.equal(40000);
      expect(await billingContract.usdPriceExcludingTax(2, 2)).to.equal(80000);
      expect(await billingContract.usdPriceExcludingTax(2, 10)).to.equal(400000);
      expect(await billingContract.usdPriceExcludingTax(2, 255)).to.equal(10200000);
    });

    it("Shoud revert for nb period === 0", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      await expect(billingContract.usdPriceExcludingTax(0, 0)).to.be.revertedWith("nbPeriods cannot be 0");
    });

    it("Shoud revert for invalid subscription period", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      await expect(billingContract.usdPriceExcludingTax(3, 0)).to.be.reverted;
    });
  });

  describe("Denied Country policy", function () {
    it("Shoud denied customer from denied country", async function () {
      const { billingContract } = await loadFixture(deployBillingFixture);
      expect(await billingContract.isDeniedCountry(112)).to.equal(true);
      expect(await billingContract.isDeniedCountry(192)).to.equal(true);
      expect(await billingContract.isDeniedCountry(180)).to.equal(true);
      expect(await billingContract.isDeniedCountry(364)).to.equal(true);
      expect(await billingContract.isDeniedCountry(368)).to.equal(true);
      expect(await billingContract.isDeniedCountry(408)).to.equal(true);
      expect(await billingContract.isDeniedCountry(729)).to.equal(true);
      expect(await billingContract.isDeniedCountry(728)).to.equal(true);
      expect(await billingContract.isDeniedCountry(729)).to.equal(true);
      expect(await billingContract.isDeniedCountry(760)).to.equal(true);
      expect(await billingContract.isDeniedCountry(716)).to.equal(true);
    });

    it("Should only billing to set denied country", async function () {
      const { billingContract, admin, deployer, otherAccount } = await loadFixture(deployBillingFixture);

      await expect(billingContract.connect(admin).setDeniedCountry(50, true)).to.be.revertedWith("not a billing");
      await expect(billingContract.connect(deployer).setDeniedCountry(50, true)).to.be.revertedWith("not a billing");
      await expect(billingContract.connect(otherAccount).setDeniedCountry(50, true)).to.be.revertedWith("not a billing");
    });

    it("Shoud denied customer after a new denied country is set", async function () {
      const { billingContract, billing } = await loadFixture(deployBillingFixture);

      expect(await billingContract.isDeniedCountry(50)).to.equal(false);
      await (await billingContract.connect(billing).setDeniedCountry(50, true)).wait();
      expect(await billingContract.isDeniedCountry(50)).to.equal(true);
      await (await billingContract.connect(billing).setDeniedCountry(50, false)).wait();
      expect(await billingContract.isDeniedCountry(50)).to.equal(false);
    });
  });
});
