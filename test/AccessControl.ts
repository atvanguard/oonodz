import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

const ADMIN_ROLE_ID = ethers.utils.keccak256(Buffer.from("ADMIN_ROLE"));

describe("AccessControl", function () {
  async function deployAccessControlFixture() {
    const [deployer, admin, billing, burner, destinationWallet, minter, proxyAdmin, refunder] = await ethers.getSigners();

    const AccessControl = await ethers.getContractFactory("AccessControl");
    const access = await AccessControl.deploy();

    const roles = {
      ADMIN: await access.ADMIN_ROLE(),
      BURNER: await access.BURNER_ROLE(),
      MINTER: await access.MINTER_ROLE(),
      BILLING: await access.BILLING_ROLE(),
      REFUNDER_ROLE: await access.REFUNDER_ROLE(),
    };

    const addresses = {
      deployer: await deployer.getAddress(),
      admin: await admin.getAddress(),
      billing: await billing.getAddress(),
      burner: await burner.getAddress(),
      minter: await minter.getAddress(),
      proxyAdmin: await proxyAdmin.getAddress(),
      refunder: await refunder.getAddress(),
    };

    await access.initialize();
    await access.grantRole(roles.ADMIN, addresses.admin);
    await access.renounceRole(roles.ADMIN, addresses.deployer);

    return { access, roles, addresses, deployer, admin, billing, burner, minter, proxyAdmin, refunder };
  }

  describe("Deployment", function () {
    it("Should define msg.sender as default admin", async function () {
      const [deployer] = await ethers.getSigners();

      const AccessControl = await ethers.getContractFactory("AccessControl");
      const access = await AccessControl.deploy();
      await access.initialize();

      expect(await access.hasRole(await access.ADMIN_ROLE(), await deployer.getAddress())).to.true;
      expect(await access.hasRole(await access.BURNER_ROLE(), await deployer.getAddress())).to.false;
      expect(await access.hasRole(await access.MINTER_ROLE(), await deployer.getAddress())).to.false;
      expect(await access.hasRole(await access.BILLING_ROLE(), await deployer.getAddress())).to.false;
    });

    it("Should define ADMIN_ROLE as admin for all roles", async function () {
      const [deployer] = await ethers.getSigners();

      const AccessControl = await ethers.getContractFactory("AccessControl");
      const access = await AccessControl.deploy();
      await access.initialize();

      expect(await access.getRoleAdmin(access.ADMIN_ROLE())).to.equals(ADMIN_ROLE_ID);
      expect(await access.getRoleAdmin(access.BURNER_ROLE())).to.equals(ADMIN_ROLE_ID);
      expect(await access.getRoleAdmin(access.MINTER_ROLE())).to.equals(ADMIN_ROLE_ID);
      expect(await access.getRoleAdmin(access.BILLING_ROLE())).to.equals(ADMIN_ROLE_ID);
    });
  });

  describe("Access management", function () {
    it("Only admin can grant/revoke roles", async function () {
      const { access, roles, addresses, admin, billing, burner, minter, proxyAdmin } = await loadFixture(
        deployAccessControlFixture
      );

      await expect(access.connect(billing).grantRole(roles.ADMIN, addresses.burner)).to.be.revertedWith(
        `AccessControl: account ${addresses.billing.toLowerCase()} is missing role ${ADMIN_ROLE_ID}`
      );
      await expect(access.connect(billing).grantRole(roles.BILLING, addresses.burner)).to.be.revertedWith(
        `AccessControl: account ${addresses.billing.toLowerCase()} is missing role ${ADMIN_ROLE_ID}`
      );
      await expect(access.connect(billing).grantRole(roles.BURNER, addresses.burner)).to.be.revertedWith(
        `AccessControl: account ${addresses.billing.toLowerCase()} is missing role ${ADMIN_ROLE_ID}`
      );
      await expect(access.connect(billing).grantRole(roles.MINTER, addresses.burner)).to.be.revertedWith(
        `AccessControl: account ${addresses.billing.toLowerCase()} is missing role ${ADMIN_ROLE_ID}`
      );
      await access.connect(admin).grantRole(roles.BILLING, addresses.billing);
      await access.connect(admin).grantRole(roles.BURNER, addresses.burner);
      await access.connect(admin).grantRole(roles.MINTER, addresses.minter);
      await access.connect(admin).grantRole(roles.ADMIN, addresses.proxyAdmin);
      await access.connect(admin).grantRole(roles.REFUNDER_ROLE, addresses.refunder);

      expect(await access.hasRole(await access.BILLING_ROLE(), addresses.billing)).to.true;
      expect(await access.hasRole(await access.BURNER_ROLE(), addresses.burner)).to.true;
      expect(await access.hasRole(await access.MINTER_ROLE(), addresses.minter)).to.true;
      expect(await access.hasRole(await access.ADMIN_ROLE(), addresses.proxyAdmin)).to.true;
      expect(await access.hasRole(await access.REFUNDER_ROLE(), addresses.refunder)).to.true;

      await access.connect(proxyAdmin).revokeRole(access.BILLING_ROLE(), addresses.billing);
      await access.connect(proxyAdmin).revokeRole(access.BURNER_ROLE(), addresses.burner);
      await access.connect(proxyAdmin).revokeRole(access.MINTER_ROLE(), addresses.minter);

      expect(await access.hasRole(await access.BILLING_ROLE(), addresses.billing)).to.false;
      expect(await access.hasRole(await access.BURNER_ROLE(), addresses.burner)).to.false;
      expect(await access.hasRole(await access.MINTER_ROLE(), addresses.minter)).to.false;
    });
  });
});
