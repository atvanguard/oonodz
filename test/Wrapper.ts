import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect, assert } from "chai";
import { ethers } from "hardhat";
import { any } from "hardhat/internal/core/params/argumentTypes";
import { time } from "@nomicfoundation/hardhat-network-helpers";

const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");

const USDT_TOKEN = "0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7";
const USDC_TOKEN = "0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E";

describe("Wrapper", function () {
  async function deployWrapperFixture() {
    const [deployer, admin, billing, burner, destinationWallet, minter, proxyAdmin, refunder, subscriber, destination, otherAccount, money] =
      await ethers.getSigners();
    const addresses = {
      deployer: await deployer.getAddress(),
      admin: await admin.getAddress(),
      billing: await billing.getAddress(),
      minter: await minter.getAddress(),
      burner: await burner.getAddress(),
      subscriber: await subscriber.getAddress(),
      destination: await destination.getAddress(),
      otherAccount: await otherAccount.getAddress(),
      money: await money.getAddress(),
      refunder: await refunder.getAddress(),
    };

    const AccessControl = await ethers.getContractFactory("AccessControl");
    const access = await AccessControl.deploy();

    const roles = {
      ADMIN: await access.ADMIN_ROLE(),
      BURNER: await access.BURNER_ROLE(),
      MINTER: await access.MINTER_ROLE(),
      BILLING: await access.BILLING_ROLE(),
      REFUNDER: await access.REFUNDER_ROLE(),
      WRAPPER: await access.WRAPPER_ROLE(),
    };

    await access.initialize();
    await access.upgrade();
    await access.grantRole(roles.ADMIN, addresses.admin);
    await access.renounceRole(roles.ADMIN, addresses.deployer);
    await access.connect(admin).grantRole(roles.BILLING, addresses.billing);
    await access.connect(admin).grantRole(roles.BURNER, addresses.burner);
    await access.connect(admin).grantRole(roles.MINTER, addresses.minter);
    await access.connect(admin).grantRole(roles.REFUNDER, addresses.refunder);

    const Billing = await ethers.getContractFactory("Billing");
    const billingContract = await Billing.deploy();
    await billingContract.initialize(access.address, USDT_TOKEN, USDC_TOKEN);

    const NFT = await ethers.getContractFactory("NodeIdNFT");
    const nft = await NFT.deploy();

    await nft.initialize(access.address);

    const Subscriptions = await ethers.getContractFactory("Subscriptions");
    const subscriptions = await Subscriptions.deploy();
    await subscriptions.initialize(access.address, billingContract.address, nft.address, addresses.destination);

    const cautionPrice = await nft.cautionPrice(); // get default caution price
    await nft.connect(admin).setSubscriptions(subscriptions.address); // Set caution price

    const usdtToken = await ethers.getContractAt("IERC20Upgradeable", USDT_TOKEN);
    const usdcToken = await ethers.getContractAt("IERC20Upgradeable", USDC_TOKEN);

    // Transfer 1.000.000 USDC to money account
    await usdcToken
      .connect(await ethers.getImpersonatedSigner("0x9f8c163cBA728e99993ABe7495F06c0A3c8Ac8b9"))
      .transfer(addresses.money, 1000000000000);

    // Transfer 1.000.000 USDT to money account
    await usdtToken
      .connect(await ethers.getImpersonatedSigner("0x9f8c163cBA728e99993ABe7495F06c0A3c8Ac8b9"))
      .transfer(addresses.money, 1000000000000);

    const Wrapper = await ethers.getContractFactory("Wrapper");
    const wrapper = await Wrapper.deploy();
    await wrapper.initialize(access.address, subscriptions.address, nft.address, billingContract.address);
    await access.connect(admin).grantRole(roles.WRAPPER, wrapper.address);

    return {
      billingContract,
      subscriptions,
      nft,
      roles,
      subscriber,
      addresses,
      deployer,
      admin,
      billing,
      minter,
      burner,
      destination,
      otherAccount,
      usdtToken,
      usdcToken,
      money,
      cautionPrice,
      refunder,
      wrapper
    };
  }

  describe("Compute optimal amounts", function () {
    const WRAPPER_TESTS = [
      {
        duration: 14,
        optimization: true,
        country: 250,
        expectedPrice: 3600,
        expectedPeriod: 0,
        expectedPrepaid: 2,
      },
      {
        duration: 30,
        optimization: true,
        country: 250,
        expectedPrice: 4800,
        expectedPeriod: 1,
        expectedPrepaid: 1,
      },
      {
        duration: 31,
        optimization: true,
        country: 250,
        expectedPrice: 8556,
        expectedPeriod: 3,
        expectedPrepaid: 31,
      },
      {
        duration: 35,
        optimization: true,
        country: 250,
        expectedPrice: 9000,
        expectedPeriod: 0,
        expectedPrepaid: 5,
      },
      {
        duration: 40,
        optimization: true,
        country: 250,
        expectedPrice: 9600,
        expectedPeriod: 1,
        expectedPrepaid: 2,
      },
      {
        duration: 120,
        optimization: true,
        country: 250,
        expectedPrice: 19200,
        expectedPeriod: 1,
        expectedPrepaid: 4,
      },
      {
        duration: 300,
        optimization: true,
        country: 250,
        expectedPrice: 48000,
        expectedPeriod: 1,
        expectedPrepaid: 10,
      },
      {
        duration: 330,
        optimization: true,
        country: 250,
        expectedPrice: 48000,
        expectedPeriod: 2,
        expectedPrepaid: 1,
      },
      {
        duration: 330,
        optimization: false,
        country: 250,
        expectedPrice: 91080,
        expectedPeriod: 3,
        expectedPrepaid: 330,
      },
      {
        duration: 365,
        optimization: true,
        country: 250,
        expectedPrice: 48000,
        expectedPeriod: 2,
        expectedPrepaid: 1,
      },
    ];
    WRAPPER_TESTS.forEach(({ duration, optimization, country, expectedPrice, expectedPeriod, expectedPrepaid }) => {
      it("Verifies that findBestRateAndPlan returns the right price and the righ plan for the provided duration", async function () {
        const { wrapper, billingContract } = await loadFixture(deployWrapperFixture);

        let amount;
        let period;
        let prepaid;

        // Expects weekly plan; 2 weeks; 36.00$
        [amount, period, prepaid] = await wrapper.findBestRateAndPlan(duration, optimization, country);
        // console.log(`${amount}, ${period}, ${prepaid}`);
        assert.equal(amount.toNumber(), expectedPrice);
        assert.equal(period, expectedPeriod);
        assert.equal(prepaid, expectedPrepaid);
      });
    });
  });

  it("Should be able wrap the entire subscription", async function () {
    const { wrapper, nft, minter, subscriber, addresses, usdtToken, subscriptions, money } = await loadFixture(deployWrapperFixture);

    await nft.connect(minter)["preMint(uint256,string)"](123, "M17yakji8RzPBFJNPXnksjoeabg7BXUJH");

    await usdtToken.connect(money).transfer(addresses.subscriber, 3000 * 1000000);
    await usdtToken.connect(subscriber).approve(wrapper.address, 3000 * 1000000);
    await subscriptions.connect(minter).addValidationSlots(1);

    // Subscribe
    await expect(await wrapper.connect(subscriber)["oneTransactionSubscription(uint16,uint16,bool,string,bool)"](250, 14, true, "USDT", true)).to.emit(subscriptions, "NewSubscription");

    expect(await subscriptions.hasActiveSubscription(addresses.subscriber, 123)).to.true;
    expect(await subscriptions.hasNodeIdCurrentSubscription(123)).to.true;
  });
});
