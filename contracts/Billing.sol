// SPDX-License-Identifier: BSL-1.1
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "./AccessControl.sol";

/// @title Billing management, including price and country authorization management
contract Billing is Initializable, RoleBasedAccessControlled {
    enum SubscriptionPeriod {
        Week, // 7 days
        Month, // 30 days
        Year, // 365 days
        Day // 1 Day
    }

    event CurrencyChanged(string symbol, address token);
    event VatRateChanged(uint16 countryISO, uint256 rate);
    event PriceRateChanged(SubscriptionPeriod period, uint256 rate);

    /// @notice List of supported currencies (example: USDT => 0x...)
    mapping(string => address) public currencies;

    /// @notice Indexed VAT rates by country (country ISO 3166 ALPHA-2)
    mapping(uint16 => uint256) public vatRates;

    /// @notice List of unauthorized countries
    mapping(uint16 => bool) public deniedCountry;

    /// @notice List of prices indexed by subscription period
    mapping(uint8 => uint256) public priceRates;

    /// @notice Public initializer with published default settings
    /// @param _accessControl The c-chain address of access control contract
    /// @param _usdtToken The c-chain address of USDT contract
    /// @param _usdtToken The c-chain address of USDC contract
    function initialize(address _accessControl, address _usdtToken, address _usdcToken) public initializer {
        RoleBasedAccessControlled.__RBAC_init(_accessControl);

        currencies["USDT"] = _usdtToken;
        currencies["USDC"] = _usdcToken;

        vatRates[40] = 20; // AT
        vatRates[56] = 21; // BE
        vatRates[100] = 20; // BG
        vatRates[196] = 19; // CY
        vatRates[203] = 21; // CZ
        vatRates[208] = 25; // DK
        vatRates[276] = 19; // DE
        vatRates[233] = 20; // EE
        vatRates[300] = 24; // GR
        vatRates[724] = 21; // ES
        vatRates[246] = 24; // FI
        vatRates[250] = 20; // FR
        vatRates[191] = 25; // HR
        vatRates[380] = 22; // IT
        vatRates[428] = 21; // LV
        vatRates[440] = 21; // LT
        vatRates[442] = 16; // LU
        vatRates[348] = 27; // HU
        vatRates[372] = 23; // IE
        vatRates[470] = 18; // MT
        vatRates[528] = 21; // NL
        vatRates[616] = 23; // PL
        vatRates[620] = 23; // PT
        vatRates[6202] = 16; // PT-20
        vatRates[6203] = 22; // PT-30
        vatRates[642] = 19; // RO
        vatRates[705] = 22; // SI
        vatRates[703] = 20; // SK
        vatRates[752] = 25; // SE

        // Set default denied country
        deniedCountry[112] = true; // Belarus
        deniedCountry[192] = true; // Cuba
        deniedCountry[180] = true; // Democratic Republic of Congo
        deniedCountry[364] = true; // Iran
        deniedCountry[368] = true; // Iraq
        deniedCountry[408] = true; // North Korea
        deniedCountry[729] = true; // Sudan
        deniedCountry[728] = true; // South Sudan
        deniedCountry[760] = true; // Syria
        deniedCountry[716] = true; // Zimbabwe
        deniedCountry[643] = true; // Russian Federation

        priceRates[uint8(SubscriptionPeriod.Day)] = 230; // 1.42 USD / day
        priceRates[uint8(SubscriptionPeriod.Week)] = 1500; // 15.00 USD / week
        priceRates[uint8(SubscriptionPeriod.Month)] = 4000; // 40.00 USD / month
        priceRates[uint8(SubscriptionPeriod.Year)] = 40000; // 400.00 USD / year
    }

    /// @notice Adds or remove a country to the list of rejected countries
    /// @param _country The country ISO 3166 ALPHA-2
    /// @param deny If true, the country is to be denied
    function setDeniedCountry(uint16 _country, bool deny) public onlyBilling {
        deniedCountry[_country] = deny;
    }

    /// @notice Modifies a country's VAT rate
    /// @param _countryIso The country ISO 3166 ALPHA-2
    /// @param _rate VAT rate (example: 20 for 20%)
    function setVatRate(uint16 _countryIso, uint256 _rate) public onlyBilling {
        require(isValidCountryCode(_countryIso), "Billing: unsupported country code");
        vatRates[_countryIso] = _rate;
        emit VatRateChanged(_countryIso, _rate);
    }

    /// @notice Adds or remove an accepted currency
    /// @param _currencySymbol The currency name (example: USDT)
    /// @param _token The currency c-chain address
    function setCurrency(string memory _currencySymbol, address _token) public onlyBilling {
        currencies[_currencySymbol] = _token;
        emit CurrencyChanged(_currencySymbol, _token);
    }

    /// @notice Modifies the price of a period
    /// @param _period The period (see SubscriptionPeriod enum)
    /// @param _rate The new rate (example: 1200 for 12.00 / two decimal)
    function setPriceRate(SubscriptionPeriod _period, uint256 _rate) public onlyBilling {
        priceRates[uint8(_period)] = _rate;
        emit PriceRateChanged(_period, _rate);
    }

    /// @notice Checks if the country is denied by the contract
    /// @param _country The country ISO 3166 ALPHA-2
    /// @return boolean True if the country is denied
    function isDeniedCountry(uint16 _country) public view returns (bool) {
        return deniedCountry[_country];
    }

    /// @notice Checks if the provided country code is valid
    /// @param _countryCode The country ISO 3166 ALPHA-2
    /// @return boolean True if country code is valid, otherwise false
    function isValidCountryCode(uint16 _countryCode) public pure returns (bool) {
        if (_countryCode > 4 && _countryCode < 975) {
            return true;
        }

        if (_countryCode == 6202 || _countryCode == 6203) {
            // Portugal - Azores or Madeira
            return true;
        }

        return false;
    }

    /// @notice Computes and returns the amount with the correct number of decimal digits
    /// @param _amount The amount with two digits (ex: for 12$ contract expects 1200)
    /// @param _symbol The currency name (ex: USDT)
    /// @return amount returns the amount with the correct number of decimal digits
    function usdToCurrency(uint256 _amount, string memory _symbol) public view returns (uint256) {
        require(isSupportedCurrency(_symbol), "Billing: unsupported currency");
        address _tokenAddress = currencies[_symbol];
        ERC20Upgradeable token = ERC20Upgradeable(_tokenAddress);

        // Warning: currently we support only USD stable coins
        // In the futur, we must convert
        return _amount * 10 ** (token.decimals() - 2);
    }

    /// @notice Returns if currency is supported
    /// @param _currencySymbol The currency name (ex: USDT)
    /// @return boolean True if supported otherwise false
    function isSupportedCurrency(string memory _currencySymbol) public view returns (bool) {
        return currencies[_currencySymbol] != address(0);
    }

    /// @notice Computes price given the period and the number of periods, excluding VAT
    /// @param _period The period (see SubscriptionPeriod enum)
    /// @param nbPeriods the number of periods (at least 1)
    /// @return price return the price excluding VAT
    function usdPriceExcludingTax(SubscriptionPeriod _period, uint256 nbPeriods) public view returns (uint256) {
        require(nbPeriods > 0, "nbPeriods cannot be 0");

        return nbPeriods * priceRates[uint8(_period)];
    }

    /// @notice Computes price VAT
    /// @param _usdAmount The USD amount
    /// @param _countryOfResidence The country ISO 3166 ALPHA-2
    /// @return price return the price VAT
    function taxAmount(uint256 _usdAmount, uint16 _countryOfResidence) public view returns (uint256) {
        uint256 rate = vatRates[_countryOfResidence];
        uint256 floatingAmount = _usdAmount * rate;
        if (floatingAmount % 100 >= 50) return floatingAmount / 100 + 1;
        else return floatingAmount / 100;
    }

    /// @notice Computes price given the period and the number of periods, including VAT
    /// @param _period The period (see SubscriptionPeriod enum)
    /// @param nbPeriods the number of periods (at least 1)
    /// @return price return the price excluding VAT
    function usdPriceIncludingTax(
        SubscriptionPeriod _period,
        uint256 nbPeriods,
        uint16 _countryOfResidence
    ) public view returns (uint256) {
        uint256 amount = usdPriceExcludingTax(_period, nbPeriods);
        uint256 tax = taxAmount(amount, _countryOfResidence);
        return (amount + tax);
    }
}
