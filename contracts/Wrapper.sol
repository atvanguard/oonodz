// SPDX-License-Identifier: BSL-1.1
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./AccessControl.sol";
import "./NodeIdNFT.sol";
import "./Subscriptions.sol";
import "./Billing.sol";

/// @title Wrapper contract wraps calls to our existing contracts to streamline our subscription flow.
contract Wrapper is Initializable, RoleBasedAccessControlled {
    /// @notice Subscriptions contract address
    Subscriptions private subscriptions;
    /// @notice NodeIdNFT contract address
    NodeIdNFT private nodeIdNFT;
    /// @notice Billing contract address
    Billing private billing;

    struct Plan {
        uint256 price;
        uint16 prepaidPeriods;
    }

    /// @notice Public initializer with published default settings
    /// @param _accessControl The c-chain address of access control contract
    /// @param _subscriptions The c-chain address of Subscription contract
    /// @param _nodeIdNFT The c-chain address of NodeidNFT contract
    /// @param _billing The c-chain address of Billing contract
    function initialize(
        address _accessControl,
        address _subscriptions,
        address _nodeIdNFT,
        address _billing
    ) public initializer {
        RoleBasedAccessControlled.__RBAC_init(_accessControl);
        subscriptions = Subscriptions(_subscriptions);
        nodeIdNFT = NodeIdNFT(_nodeIdNFT);
        billing = Billing(_billing);
    }

    /// @notice oneTransactionSubscription streamlines the subscrition process (to be called by integrator smart contract)
    /// @param customer Customer's address.
    /// @param countryOfResidence Customer's country of residence, used to compute VAT if applicable.
    /// @param duration The duration in days.
    /// @param bestRate Indicates whether the customer wishes to pay for the best rate but lose refund rights.
    /// @param currencySymbol Name of ERC-20 token to be used to pay for subscription
    /// @param withdrawalRightWaiver Require to be true, customer has to waive his right.
    function oneTransactionSubscription(
        address customer,
        uint16 countryOfResidence,
        uint16 duration,
        bool bestRate,
        string memory currencySymbol,
        bool withdrawalRightWaiver
    ) public onlyIntegrator returns (address) {
        (uint256 amount, Billing.SubscriptionPeriod period, uint16 prepaidPeriods) = findBestRateAndPlan(
            duration,
            bestRate,
            countryOfResidence
        );
        return
            _oneTransactionSubscription(
                customer,
                countryOfResidence,
                period,
                prepaidPeriods,
                amount,
                currencySymbol,
                withdrawalRightWaiver
            );
    }

    /// @notice oneTransactionSubscription streamlines the subscrition process (to be called directly by the user)
    /// @param countryOfResidence User's country of residence, used to compute VAT if applicable.
    /// @param duration The duration in days.
    /// @param bestRate Indicates whether the user wishes to pay for the best rate but lose refund rights.
    /// @param currencySymbol Name of ERC-20 token to be used to pay for subscription
    /// @param withdrawalRightWaiver Require to be true, user has to waive his right.
    function oneTransactionSubscription(
        uint16 countryOfResidence,
        uint16 duration,
        bool bestRate,
        string memory currencySymbol,
        bool withdrawalRightWaiver
    ) public returns (address) {
        (uint256 amount, Billing.SubscriptionPeriod period, uint16 prepaidPeriods) = findBestRateAndPlan(
            duration,
            bestRate,
            countryOfResidence
        );
        return
            _oneTransactionSubscription(
                _msgSender(),
                countryOfResidence,
                period,
                prepaidPeriods,
                amount,
                currencySymbol,
                withdrawalRightWaiver
            );
    }

    /// @notice Streamlines the subscrition process
    function _oneTransactionSubscription(
        address customer,
        uint16 countryOfResidence,
        Billing.SubscriptionPeriod period,
        uint16 prepaidPeriods,
        uint256 amount,
        string memory currencySymbol,
        bool withdrawalRightWaiver
    ) private returns (address) {
        // Register
        subscriptions.register(countryOfResidence, customer);

        // Mint a new NodeID
        uint256 tokenid = nodeIdNFT.mint(customer);

        // Transfer money from client or upper contract
        transferMoney(amount, currencySymbol);

        // newSubscription
        subscriptions.newSubscription(period, tokenid, currencySymbol, prepaidPeriods, withdrawalRightWaiver, customer);

        // return nodeid and tokenid
        return nodeIdNFT.base58Decode(nodeIdNFT.nodeIds(tokenid));
    }

    function transferMoney(uint256 amount, string memory currencySymbol) private {
        // Add decimals according to currency.decimals
        uint256 totalAmount = billing.usdToCurrency(amount, currencySymbol);

        // Check that Smart Contract is allowed to transfer total amount
        IERC20Upgradeable token = IERC20Upgradeable(billing.currencies(currencySymbol));
        require(token.allowance(_msgSender(), address(this)) >= totalAmount, "not enough allowance");

        // Transfer amount to wrapper contract
        require(token.transferFrom(_msgSender(), address(this), totalAmount), "error occurred during transferFrom");

        // Approve subscription contract to withdraw the amount
        require(
            token.approve(address(subscriptions), totalAmount),
            "error occurred during approve from wrapper to subscription contract"
        );
    }

    /**
     * @notice Find and return the best rate plan for a specified duration.
     * @dev If not bestRate, the function as dummy and return daily plan with the specified duration.
     * @param duration The duration in days to find a rate plan for.
     * @return bestRate If true, return the best plan, otherwise returns daily plan.
     * @return countryOfResidence User's or Customer's country of residence to compute VAT rate (if applicable).
     */
    function findBestRateAndPlan(
        uint16 duration,
        bool bestRate,
        uint16 countryOfResidence
    ) public view returns (uint256, Billing.SubscriptionPeriod, uint16) {
        Plan[] memory plans = new Plan[](4);
        plans[uint256(Billing.SubscriptionPeriod.Day)].price = billing.usdPriceExcludingTax(
            Billing.SubscriptionPeriod.Day,
            duration
        );
        plans[uint256(Billing.SubscriptionPeriod.Day)].prepaidPeriods = duration;
        if (bestRate == false)
            return (
                plans[uint256(Billing.SubscriptionPeriod.Day)].price +
                    billing.taxAmount(plans[uint256(Billing.SubscriptionPeriod.Day)].price, countryOfResidence),
                Billing.SubscriptionPeriod.Day,
                duration
            );

        plans[uint256(Billing.SubscriptionPeriod.Week)].prepaidPeriods = 1;
        plans[uint256(Billing.SubscriptionPeriod.Month)].prepaidPeriods = 1;
        plans[uint256(Billing.SubscriptionPeriod.Year)].prepaidPeriods = 1;
        plans[uint256(Billing.SubscriptionPeriod.Year)].price = billing.usdPriceExcludingTax(
            Billing.SubscriptionPeriod.Year,
            plans[uint256(Billing.SubscriptionPeriod.Year)].prepaidPeriods
        );
        if (duration > 13) {
            plans[uint256(Billing.SubscriptionPeriod.Week)].prepaidPeriods = duration / 7; // use div not mod
            if (plans[uint256(Billing.SubscriptionPeriod.Week)].prepaidPeriods * 7 < duration)
                plans[uint256(Billing.SubscriptionPeriod.Week)].prepaidPeriods++;
        }
        plans[uint256(Billing.SubscriptionPeriod.Week)].price = billing.usdPriceExcludingTax(
            Billing.SubscriptionPeriod.Week,
            plans[uint256(Billing.SubscriptionPeriod.Week)].prepaidPeriods
        );
        if (duration > 29) {
            plans[uint256(Billing.SubscriptionPeriod.Month)].prepaidPeriods = duration / 30; // use div not mod
            if (plans[uint256(Billing.SubscriptionPeriod.Month)].prepaidPeriods * 30 < duration)
                plans[uint256(Billing.SubscriptionPeriod.Month)].prepaidPeriods++;
        }
        plans[uint256(Billing.SubscriptionPeriod.Month)].price = billing.usdPriceExcludingTax(
            Billing.SubscriptionPeriod.Month,
            plans[uint256(Billing.SubscriptionPeriod.Month)].prepaidPeriods
        );

        uint256 lowestPlan = uint256(Billing.SubscriptionPeriod.Day);

        for (uint256 i = 0; i < plans.length; i++) {
            if (plans[i].price < plans[lowestPlan].price) {
                lowestPlan = i;
            }
        }

        return (
            plans[lowestPlan].price + billing.taxAmount(plans[lowestPlan].price, countryOfResidence),
            Billing.SubscriptionPeriod(lowestPlan),
            plans[lowestPlan].prepaidPeriods
        );
    }
}
