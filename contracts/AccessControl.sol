// SPDX-License-Identifier: BSL-1.1
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";

contract AccessControl is AccessControlUpgradeable {
    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant BILLING_ROLE = keccak256("BILLING_ROLE");
    bytes32 public constant REFUNDER_ROLE = keccak256("REFUNDER_ROLE");
    bytes32 public constant WRAPPER_ROLE = keccak256("WRAPPER_ROLE");
    bytes32 public constant INTEGRATOR_ROLE = keccak256("INTEGRATOR_ROLE");

    function initialize() public initializer {
        _setupRole(ADMIN_ROLE, _msgSender());
        _setRoleAdmin(ADMIN_ROLE, ADMIN_ROLE);
        _setRoleAdmin(MINTER_ROLE, ADMIN_ROLE);
        _setRoleAdmin(BURNER_ROLE, ADMIN_ROLE);
        _setRoleAdmin(BILLING_ROLE, ADMIN_ROLE);
        _setRoleAdmin(REFUNDER_ROLE, ADMIN_ROLE);
        _setRoleAdmin(WRAPPER_ROLE, ADMIN_ROLE);
        _setRoleAdmin(INTEGRATOR_ROLE, ADMIN_ROLE);
    }

    function upgrade() public {
        _setRoleAdmin(WRAPPER_ROLE, ADMIN_ROLE);
        _setRoleAdmin(INTEGRATOR_ROLE, ADMIN_ROLE);
    }
}

contract RoleBasedAccessControlled is Initializable, ContextUpgradeable {
    AccessControl private access;

    // solhint-disable-next-line func-name-mixedcase
    function __RBAC_init(address _accessControl) internal onlyInitializing {
        access = AccessControl(_accessControl);
    }

    modifier onlyAdmin() {
        require(access.hasRole(access.ADMIN_ROLE(), _msgSender()), "not an admin");
        _;
    }

    modifier onlyMinter() {
        require(access.hasRole(access.MINTER_ROLE(), _msgSender()), "not a minter");
        _;
    }

    modifier onlyBurner() {
        require(access.hasRole(access.BURNER_ROLE(), _msgSender()), "not a burner");
        _;
    }

    modifier onlyBilling() {
        require(access.hasRole(access.BILLING_ROLE(), _msgSender()), "not a billing");
        _;
    }

    modifier onlyRefunder() {
        require(access.hasRole(access.REFUNDER_ROLE(), _msgSender()), "not a refunder");
        _;
    }

    modifier onlyWrapper() {
        require(access.hasRole(access.WRAPPER_ROLE(), _msgSender()), "not a wrapper");
        _;
    }

    modifier onlyIntegrator() {
        require(access.hasRole(access.INTEGRATOR_ROLE(), _msgSender()), "not a integrator");
        _;
    }

    function isAdmin(address _address) internal view returns (bool) {
        return access.hasRole(access.ADMIN_ROLE(), _address);
    }

    function isMinter(address _address) internal view returns (bool) {
        return access.hasRole(access.MINTER_ROLE(), _address);
    }

    function isBurner(address _address) internal view returns (bool) {
        return access.hasRole(access.BURNER_ROLE(), _address);
    }

    function isBilling(address _address) internal view returns (bool) {
        return access.hasRole(access.BILLING_ROLE(), _address);
    }

    function isRefunder(address _address) internal view returns (bool) {
        return access.hasRole(access.REFUNDER_ROLE(), _address);
    }

    function isWrapper(address _address) internal view returns (bool) {
        return access.hasRole(access.WRAPPER_ROLE(), _address);
    }

    function isIntegrator(address _address) internal view returns (bool) {
        return access.hasRole(access.INTEGRATOR_ROLE(), _address);
    }

    uint256[49] private __gap;
}
