// SPDX-License-Identifier: BSL-1.1
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./AccessControl.sol";
import "./Subscriptions.sol";

// Uncomment this line to use console.log
// import "hardhat/console.sol";

/// @title NodeID NFT contract that manage NodeID unicity and property
contract NodeIdNFT is ERC721EnumerableUpgradeable, RoleBasedAccessControlled {
    event NewPremintedNFT(uint256 _tokenId, string _nodeId);
    event NewMintedNFT(uint256 _tokenId, address _owner, string _nodeId);
    event BurnNFT(uint256 _tokenId);

    struct Deposit {
        bool isUsed;
        uint256 value;
    }

    /// @notice Each NFT as its own NodeID
    mapping(uint256 => string) public nodeIds;

    /// @notice To prevent a single user from recovering all the nft generated, the user must submit a deposit.
    mapping(address => Deposit[]) private depositedCaution;

    /// @notice The node identifier (NodeID) must remain unique among all NFTs
    mapping(bytes32 => uint256) private hashedNodeIds;

    /// @notice Preminted NFT
    uint256[] public preMintedTokenId;

    /// @notice Preminted blank NFT used to import a staker.key/staker.crt
    uint256[] public preMintedBlankTokenId;

    /// @notice Subscriptions contract address
    address private subscriptions;

    /// @notice Deposit price in avax
    uint256 public cautionPrice;

    /// @notice Wrapper address
    address private wrapper;

    /// @notice Base58 alphabet
    bytes private constant ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

    /// @notice Public initializer with published default settings
    /// @param _accessControl The c-chain address of access control contract
    function initialize(address _accessControl) public initializer {
        RoleBasedAccessControlled.__RBAC_init(_accessControl);
        ERC721Upgradeable.__ERC721_init("NodeIdNFT", "NID");
        ERC721EnumerableUpgradeable.__ERC721Enumerable_init();

        cautionPrice = 2 * 10 ** 18; // Set deposit price to 2 avax
    }

    /// @notice To deposit caution before calling mint or mintblank function
    function deposit() public payable {
        require(msg.value == cautionPrice, "NodeIdNFT: You must deposit the exact caution price (see getCautionPrice)");

        Deposit memory _deposit = Deposit(false, msg.value);
        depositedCaution[_msgSender()].push(_deposit);
    }

    /// @notice Set subcriptions contract address
    /// @param _subscriptions subscription contract address
    function setSubscriptions(address _subscriptions) public onlyAdmin {
        subscriptions = _subscriptions;
    }

    /// @notice Premint an nft with a nodeid
    /// @dev Only addresses with the role MINTER_ROLE can use this function
    /// @param tokenId token / nft identifier
    /// @param nodeId avalanche NodeID (without "NodeID-")
    function preMint(uint256 tokenId, string memory nodeId) public onlyMinter {
        require(tokenId != 0, "NodeIdNFT: tokendId 0 is reserved"); // otherwise, _existNodeId won't work
        require(_existNodeId(nodeId) == false, "NodeIdNFT: NodeID is already registered");

        _safeMint(_msgSender(), tokenId, "");

        nodeIds[tokenId] = nodeId;
        preMintedTokenId.push(tokenId);
        hashedNodeIds[keccak256(abi.encodePacked(nodeId))] = tokenId;

        emit NewPremintedNFT(tokenId, nodeId);
    }

    /// @notice Premint a blank NFT
    /// @dev NFT blanks are used to let users import their keys.
    /// @param tokenId token / nft identifier
    function preMint(uint256 tokenId) public onlyMinter {
        require(tokenId != 0, "NodeIdNFT: tokendId 0 is reserved"); // otherwise, _existNodeId won't work

        _safeMint(_msgSender(), tokenId, "");
        preMintedBlankTokenId.push(tokenId);

        emit NewPremintedNFT(tokenId, "");
    }

    /// @notice This function defines the nodeid of a blank token. Must be called by our backend after it has received the staker.key and fully secured it.
    /// @param tokenId token / nft identifier
    /// @param nodeId avalanche NodeID (without "NodeID-")
    function setNodeID(uint256 tokenId, string memory nodeId) public onlyMinter {
        // NFT has to be minted and NodeID not set
        _requireMinted(tokenId);
        require(
            keccak256(abi.encodePacked(nodeIds[tokenId])) == keccak256(abi.encodePacked("")),
            "NodeIdNFT: NodeID is already field, can't edit it!"
        );
        require(_existNodeId(nodeId) == false, "NodeIdNFT: NodeID is already registered");

        nodeIds[tokenId] = nodeId;
        hashedNodeIds[keccak256(abi.encodePacked(nodeId))] = tokenId;

        emit NewMintedNFT(tokenId, ownerOf(tokenId), nodeId);
    }

    /// @notice Deposit price could be raised or lowered
    /// @dev The deposit is used as protection against attacks. The aim is to prevent a malicious actor from collecting all NFTs that have no commercial value.
    /// @param _caution new deposit price (example: 2000000000000000000 equals 2 avax)
    function setCautionPrice(uint256 _caution) public onlyBilling {
        cautionPrice = _caution;
    }

    /// @notice Refund one deposit after a user completes its subscription
    /// @dev Public function as we allow billing to refund one user off the actual process
    /// @param user user's address
    function refund(address payable user) public {
        require(
            _msgSender() == subscriptions || isRefunder(_msgSender()),
            "NodeIdNFT: caller is not a refunder user nor subscription contrat"
        );

        require(depositedCaution[user].length > 0, "NodeIdNFT: No caution left to refund");

        uint256 _index = 0;
        uint256 __refund = 0;

        for (uint256 i = 0; i < depositedCaution[user].length; i++) {
            if (depositedCaution[user][i].isUsed == true) {
                _index = i;
                break;
            }
        }

        __refund = depositedCaution[user][_index].value;

        depositedCaution[user][_index] = depositedCaution[user][depositedCaution[user].length - 1];
        depositedCaution[user].pop();

        user.transfer(__refund);
    }

    /// @notice Burn specified tokenId
    /// @dev Only owner / approved account or address with role BURNER_ROLE can use this function
    /// @param tokenId token / nft identifier
    function burn(uint256 tokenId) public {
        require(
            _isApprovedOrOwner(_msgSender(), tokenId) || isBurner(_msgSender()),
            "NodeIdNFT: caller is not token owner nor approved nor a burner"
        );
        Subscriptions _subscription = Subscriptions(subscriptions);
        require(_subscription.hasNodeIdCurrentSubscription(tokenId) == false, "NodeIdNFT: Can't burn an active NodeID");

        if (_subscription.customerExists(_msgSender())) {
            require(
                _subscription.hasPendingRefund(_msgSender(), tokenId) == false,
                "NodeIdNFT: there is a pending refund, can't burn the token"
            );
        }

        _burn(tokenId);
        nodeIds[tokenId] = "";
        hashedNodeIds[keccak256(abi.encodePacked(""))] = tokenId;

        emit BurnNFT(tokenId);
    }

    /// @notice Allows users to mint a nodeid nft
    function mint() public returns (uint256) {
        require(
            getFreeDepositOf(_msgSender()) > 0,
            "NodeIdNFT: No deposit! You must deposit a caution prior minting a node ID"
        );
        require(consumeDepositOf(_msgSender()), "NodeIdNFT: Can't consume a caution prior minting you NFT");

        return _mint(_msgSender());
    }

    function mint(address customer) public onlyWrapper returns (uint256) {
        return _mint(customer);
    }

    function _mint(address customer) private returns (uint256) {
        require(preMintedTokenId.length > 0, "NodeIdNFT: No premint token available");

        uint256 _tokenId;
        _tokenId = preMintedTokenId[preMintedTokenId.length - 1];
        address owner = ownerOf(_tokenId);
        _transfer(owner, customer, _tokenId);
        preMintedTokenId.pop();

        emit NewMintedNFT(_tokenId, customer, nodeIds[_tokenId]);
        return _tokenId;
    }

    /// @notice Allows users to mint a blank nft (used only to import an existing staker.key)
    function mintBlank() public {
        uint256 _tokenId;
        require(preMintedBlankTokenId.length > 0, "NodeIdNFT: No premint token available");
        require(
            getFreeDepositOf(_msgSender()) > 0,
            "NodeIdNFT: No deposit! You must deposit a caution prior minting a node ID"
        );

        require(consumeDepositOf(_msgSender()), "NodeIdNFT: Can't consume a caution prior minting you NFT");
        _tokenId = preMintedBlankTokenId[preMintedBlankTokenId.length - 1];
        address owner = ownerOf(_tokenId);
        _transfer(owner, _msgSender(), _tokenId);
        preMintedBlankTokenId.pop();

        emit NewMintedNFT(_tokenId, _msgSender(), "");
    }

    /// @notice Get how many preminted tokens are availables
    /// @return count The number of preminted tokens
    function getAvailablePreMint() public view returns (uint256) {
        return preMintedTokenId.length;
    }

    /// @notice Get how many preminted blank tokens are availables
    /// @return Count The number of preminted blank tokens
    function getAvailablePreMintBlank() public view returns (uint256) {
        return preMintedBlankTokenId.length;
    }

    /// @notice Get how much a user has deposit (avax)
    /// @param user User's address
    /// @return balance The number of avax a user has deposited
    function getDepositBalanceOf(address user) public view returns (uint256) {
        require(user != address(0), "NodeIdNFT: address zero is not a valid owner");
        uint256 _balance = 0;

        for (uint256 i = 0; i < depositedCaution[user].length; i++) {
            _balance += depositedCaution[user][i].value;
        }

        return _balance;
    }

    /// @notice Get how much a user can claim (avax)
    /// @param user User's address
    /// @return balance The number of avax a user can claim
    function getClaimableBalanceOf(address user) public view returns (uint256) {
        require(user != address(0), "NodeIdNFT: address zero is not a valid owner");
        uint256 _balance = 0;

        for (uint256 i = 0; i < depositedCaution[user].length; i++) {
            if (depositedCaution[user][i].isUsed == true) {
                _balance += depositedCaution[user][i].value;
            }
        }

        return _balance;
    }

    /// @notice Get how many deposit(s) a user has paid and not used
    /// @param user User's address
    /// @return nbDeposit The number of available deposits. In other words, the number of nft a user can request.
    /// @dev Variable nbDeposit is the deposit price multiplier. getFreeDepositOf(user) * cautionPrice = getDepositBalanceOf(user) - getClaimableBalanceOf(user)
    function getFreeDepositOf(address user) public view returns (uint256) {
        require(user != address(0), "NodeIdNFT: address zero is not a valid owner");
        if (depositedCaution[user].length > 0) {
            uint256 _nbDeposit = 0;
            for (uint256 i = 0; i < depositedCaution[user].length; i++) {
                if (depositedCaution[user][i].isUsed == false) {
                    _nbDeposit++;
                }
            }
            return _nbDeposit;
        }
        return 0;
    }

    /// @notice base58 decode nodeid to an address
    /// @param nodeid string nodeid
    /// @return address an nodeid as an address
    function base58Decode(string memory nodeid) public pure returns (address) {
        bytes memory buff = decode(bytes(nodeid));
        require(buff.length == 24, "NodeIdNFT: invalid nodeid address");
        // The buffer is 24 bytes long: 20 bytes body + 4 bytes checksum.
        // The checksum is removed and the body is converted to an address.
        return address(uint160(bytes20(buff)));
    }

    /**
     * @notice decode is used to decode the given string in base58 standard.
     * @param data_ data encoded with base58, passed in as bytes.
     * @return raw data, returned as bytes.
     * @dev Code imported from https://github.com/storyicon/base58-solidity/
     */
    function decode(bytes memory data_) private pure returns (bytes memory) {
        unchecked {
            uint256 zero = 49;
            uint256 b58sz = data_.length;
            uint256 zcount = 0;
            for (uint256 i = 0; i < b58sz && uint8(data_[i]) == zero; i++) {
                zcount++;
            }
            uint256 t;
            uint256 c;
            bool f;
            bytes memory binu = new bytes(2 * (((b58sz * 8351) / 6115) + 1));
            uint32[] memory outi = new uint32[]((b58sz + 3) / 4);
            for (uint256 i = 0; i < data_.length; i++) {
                bytes1 r = data_[i];
                (c, f) = indexOf(ALPHABET, r);
                require(f, "invalid base58 digit");
                for (int256 k = int256(outi.length) - 1; k >= 0; k--) {
                    t = uint64(outi[uint256(k)]) * 58 + c;
                    c = t >> 32;
                    outi[uint256(k)] = uint32(t & 0xffffffff);
                }
            }
            uint64 mask = uint64(b58sz % 4) * 8;
            if (mask == 0) {
                mask = 32;
            }
            mask -= 8;
            uint256 outLen = 0;
            for (uint256 j = 0; j < outi.length; j++) {
                while (mask < 32) {
                    binu[outLen] = bytes1(uint8(outi[j] >> mask));
                    outLen++;
                    if (mask < 8) {
                        break;
                    }
                    mask -= 8;
                }
                mask = 24;
            }
            for (uint256 msb = zcount; msb < binu.length; msb++) {
                if (binu[msb] > 0) {
                    return slice(binu, msb - zcount, outLen);
                }
            }
            return slice(binu, 0, outLen);
        }
    }

    /**
     * @notice slice is used to slice the given byte, returns the bytes in the range of [start_, end_].
     * @param data_ raw data, passed in as bytes.
     * @param start_ start index.
     * @param end_ end index.
     * @return slice data
     * @dev Code imported from https://github.com/storyicon/base58-solidity/
     */
    function slice(bytes memory data_, uint256 start_, uint256 end_) private pure returns (bytes memory) {
        unchecked {
            bytes memory ret = new bytes(end_ - start_);
            for (uint256 i = 0; i < end_ - start_; i++) {
                ret[i] = data_[i + start_];
            }
            return ret;
        }
    }

    /**
     * @notice indexOf is used to find where char_ appears in data_.
     * @param data_ raw data, passed in as bytes.
     * @param char_ target byte.
     * @return index, and whether the search was successful.
     * @dev Code imported from https://github.com/storyicon/base58-solidity/
     */
    function indexOf(bytes memory data_, bytes1 char_) private pure returns (uint256, bool) {
        unchecked {
            for (uint256 i = 0; i < data_.length; i++) {
                if (data_[i] == char_) {
                    return (i, true);
                }
            }
            return (0, false);
        }
    }

    /// @notice Consumes one user deposit
    /// @param user User's address
    /// @return boolean Returns true if a deposit was used, otherwise false
    function consumeDepositOf(address user) internal returns (bool) {
        if (depositedCaution[user].length > 0) {
            for (uint256 i = 0; i < depositedCaution[user].length; i++) {
                if (depositedCaution[user][i].isUsed == false) {
                    depositedCaution[user][i].isUsed = true;
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    /// @notice Returns baseURI
    /// @return uri Returns baseURI
    function _baseURI() internal pure override returns (string memory) {
        return "https://app.nodz.network/";
    }

    /// @notice checks if a NodeID exists
    /// @return boolean Returns true if NodeID exists, otherwise false
    /// @dev We've implemented this function to ensure that a NodeID remains unique among all NFTs
    function _existNodeId(string memory nodeId) private view returns (bool) {
        return hashedNodeIds[keccak256(abi.encodePacked(nodeId))] != 0;
    }
}
