// SPDX-License-Identifier: BSL-1.1
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "./AccessControl.sol";
import "./Billing.sol";
import "./NodeIdNFT.sol";

/// @title The subscription contract manages user subscriptions, VAT and non-authorized countries.
contract Subscriptions is Initializable, RoleBasedAccessControlled {
    event NewCustomerRegistered(address _wallet, uint16 _countryOfResidence);
    event NewSubscription(
        address _wallet,
        uint256 _tokenId,
        Billing.SubscriptionPeriod _period,
        uint16 _prepaidPeriods,
        string _currency,
        uint256 _startTs,
        uint256 _endTs,
        uint256 _usdPrice,
        uint256 _usdTaxes,
        uint256 _priceRate,
        uint256 _vatRate
    );
    event ReloadSubscription(
        address _wallet,
        uint256 _tokenId,
        Billing.SubscriptionPeriod _period,
        uint16 _addedPeriods,
        string _currency,
        uint256 _startTs,
        uint256 _endTs,
        uint256 _usdPrice,
        uint256 _usdTaxes,
        uint256 _priceRate,
        uint256 _vatRate
    );
    event StopSubscription(address _wallet, uint256 _tokenId, uint256 _startTs, uint256 _endTs, uint256 _blocktime);
    event DestinationWalletChanged(address _oldWallet, address _newWallet);
    event NewRefund(
        address _wallet,
        uint256 _tokenId,
        uint256 _startTs,
        string _currency,
        uint256 _amount,
        uint256 _refundPeriods
    );
    event RefundCompleted(address _wallet, address _payer, uint256 _tokenId, string _currency, uint256 _amount);

    Billing private billing;
    NodeIdNFT private nodeIdToken;
    address public destinationWallet;
    uint256 public validationSlots;
    uint16 public gtcVersion;

    struct Customer {
        bool registered;
        uint16 countryOfResidence;
        uint16 acceptedGtcVersion;
    }

    struct Subscription {
        uint256 startTs;
        uint256 endTs;
        Billing.SubscriptionPeriod period;
        string currencySymbol;
        uint256 priceRate;
        uint256 vatRate;
    }

    struct Refund {
        bool pending;
        string currencySymbol;
        uint256 amount;
    }

    mapping(address => Customer) public customers;

    /// @dev One customer can have multiple active subscriptions, but only for different node id
    /// @notice user's subscriptions
    // NOTE: the SC currently does NOT store history of previous subscriptions
    // NOTE: the SC currently does NOT provide a way to enumerate all subscriptions for a Customer
    mapping(address => mapping(uint256 => Subscription)) public subscriptions;

    /// @notice User's refunds
    mapping(address => mapping(uint256 => Refund)) public refunds;

    /// @notice Gives the subscription end date of a nodeid
    mapping(uint256 => uint256) private nodeidFreeAfter;

    /// @notice Public initializer with published default settings
    /// @param _accessControl The c-chain address of access control contract
    /// @param _billing The c-chain address of billing contract
    /// @param _nodeIdToken The c-chain address of NodeidNFT contract
    /// @param _destinationWallet The c-chain address of the wallet that collects user payments
    function initialize(
        address _accessControl,
        address _billing,
        address _nodeIdToken,
        address _destinationWallet
    ) public initializer {
        RoleBasedAccessControlled.__RBAC_init(_accessControl);
        billing = Billing(_billing);
        nodeIdToken = NodeIdNFT(_nodeIdToken);
        destinationWallet = _destinationWallet;
    }

    /// @notice Set GTC version number
    /// @dev users must accept the latest version
    function setGtcVersion(uint16 _version) public onlyAdmin {
        require(_version > gtcVersion, "new version must be highier than the previous one");
        gtcVersion = _version;
    }

    /// @notice Modifies the destination wallet address that collects user payments
    /// @param _newDestinationWallet The destination wallet address
    function changeDestationWallet(address _newDestinationWallet) public onlyAdmin {
        require(_newDestinationWallet != destinationWallet, "same wallet");
        address _old = destinationWallet;
        destinationWallet = _newDestinationWallet;
        emit DestinationWalletChanged(_old, _newDestinationWallet);
    }

    /// @notice Adds available validation slots
    /// @param _slots Number of slots to add
    function addValidationSlots(uint256 _slots) public onlyMinter {
        validationSlots += _slots;
    }

    /// @notice Removes available validation slots
    /// @param _slots Number of slots to remove
    function removeValidationSlots(uint256 _slots) public onlyMinter {
        validationSlots -= _slots;
    }

    /// @notice Refund user after a subscription
    /// @param _wallet The wallet address to refund
    /// @param _tokenId The NodeidNFT identifier associated to the refund
    function completeRefund(address _wallet, uint256 _tokenId) public onlyRefunder {
        // Check that customer has an active subscription for this nodeId
        require(hasPendingRefund(_wallet, _tokenId) == true, "no pending refund for this wallet & node id");

        Refund memory refund = refunds[_wallet][_tokenId];
        uint256 _amount = billing.usdToCurrency(refund.amount, refund.currencySymbol);

        // Check that Smart Contract is allowed to transfer total amount
        IERC20Upgradeable token = IERC20Upgradeable(billing.currencies(refund.currencySymbol));
        require(token.allowance(_msgSender(), address(this)) >= _amount, "not enough allowance");

        // Change refund status
        refunds[_wallet][_tokenId].pending = false;

        // Transfer tokens to customer
        require(token.transferFrom(_msgSender(), _wallet, _amount), "error occurred during transferFrom");

        emit RefundCompleted(_wallet, _msgSender(), _tokenId, refund.currencySymbol, refund.amount);
    }

    function register(uint16 _countryOfResidence) public {
        _register(_countryOfResidence, _msgSender());
    }

    function register(uint16 _countryOfResidence, address customer) public onlyWrapper {
        _register(_countryOfResidence, customer);
    }

    /// @notice Registers a user that implicitly accept GTC at the same time
    /// @param _countryOfResidence The user's country of residence, which will be used to calculate VAT
    function _register(uint16 _countryOfResidence, address customer) private {
        require(billing.isValidCountryCode(_countryOfResidence), "Unsupported country code");
        require(billing.isDeniedCountry(_countryOfResidence) == false, "Country is denied by our policy");

        customers[customer].countryOfResidence = _countryOfResidence;
        customers[customer].registered = true;
        customers[customer].acceptedGtcVersion = gtcVersion;

        emit NewCustomerRegistered(customer, _countryOfResidence);
    }

    function newSubscription(
        Billing.SubscriptionPeriod _period,
        uint256 _tokenId,
        string memory _currencySymbol,
        uint16 _prepaidPeriods, // max 255 prepaid periods
        bool _withdrawalRightWaiver
    ) public {
        _newSubscriptionRequirements(_tokenId, _currencySymbol, _prepaidPeriods, _withdrawalRightWaiver, _msgSender());
        _newSubscription(_period, _tokenId, _currencySymbol, _prepaidPeriods, _msgSender());
    }

    function newSubscription(
        Billing.SubscriptionPeriod _period,
        uint256 _tokenId,
        string memory _currencySymbol,
        uint16 _prepaidPeriods, // max 255 prepaid periods
        bool _withdrawalRightWaiver,
        address customer
    ) public onlyWrapper {
        _newSubscriptionRequirements(_tokenId, _currencySymbol, _prepaidPeriods, _withdrawalRightWaiver, customer);
        _newSubscription(_period, _tokenId, _currencySymbol, _prepaidPeriods, customer);
    }

    function _newSubscriptionRequirements(
        uint256 _tokenId,
        string memory _currencySymbol,
        uint16 _prepaidPeriods, // max 255 prepaid periods
        bool _withdrawalRightWaiver,
        address customer
    ) private view {
        // Require that customers waives its 14 days to withdraw right
        require(_withdrawalRightWaiver == true, "You must waive your 14 days withdrawal right");

        // Check that customer is already registered and has accepted latest GTC version
        require(customerExists(customer), "not a known customer");
        require(customers[customer].acceptedGtcVersion == gtcVersion, "You must accept our new GTC");

        require(
            billing.isDeniedCountry(customers[customer].countryOfResidence) == false,
            "Your country of residence is denied by our policy"
        );

        // Nb prepaid periods must be > 0
        require(_prepaidPeriods > 0, "nb of prepaid periods must be at least 1");

        require(nodeIdToken.ownerOf(_tokenId) == customer, "not owner of nodeId");

        require(
            keccak256(abi.encodePacked(nodeIdToken.nodeIds(_tokenId))) != keccak256(abi.encodePacked("")),
            "nodeId is void"
        );

        require(
            hasActiveSubscription(customer, _tokenId) == false,
            "already has an active subscription for this node id"
        );

        require(hasNodeIdCurrentSubscription(_tokenId) == false, "nodeid already has a subscription");

        // Check that currency is supported
        require(billing.isSupportedCurrency(_currencySymbol), "not a supported currency");

        require(validationSlots > 0, "no validation slot available, come back later");
    }

    /// @notice Allows a user to order a validator
    /// @param _period The subscription period (weekly, monthly, yearly)
    /// @param _tokenId The NodeidNFT identifier
    /// @param _currencySymbol The currency name used to pay the subscription
    /// @param _prepaidPeriods The number of periods a user wants to subscribe
    function _newSubscription(
        Billing.SubscriptionPeriod _period,
        uint256 _tokenId,
        string memory _currencySymbol,
        uint16 _prepaidPeriods, // max 65 536 prepaid periods
        address customer
    ) private {
        // Query everything
        uint256 usdPriceForSubscriptionExclTax = billing.usdPriceExcludingTax(_period, _prepaidPeriods);
        uint256 usdTax = billing.taxAmount(usdPriceForSubscriptionExclTax, customers[customer].countryOfResidence);
        uint256 totalTokenAmount = billing.usdToCurrency(usdPriceForSubscriptionExclTax + usdTax, _currencySymbol);
        uint256 _priceRate = billing.priceRates(uint8(_period));
        uint256 _vatRate = billing.vatRates(customers[_msgSender()].countryOfResidence);

        // Check that Smart Contract is allowed to transfer total amount
        IERC20Upgradeable token = IERC20Upgradeable(billing.currencies(_currencySymbol));
        require(token.allowance(_msgSender(), address(this)) >= totalTokenAmount, "not enough allowance");

        /*
         * => https://docs.soliditylang.org/en/v0.8.17/units-and-global-variables.html
         * The current block timestamp must be strictly larger than the timestamp of the last block,
         * but the only guarantee is that it will be somewhere between the timestamps of two consecutive blocks
         * in the canonical chain.
         */
        /* solhint-disable-next-line not-rely-on-time  */
        uint256 _startTs = block.timestamp;
        uint256 _endTs = _startTs;

        if (_period == Billing.SubscriptionPeriod.Day) {
            _endTs = _startTs + (1 days) * uint256(_prepaidPeriods);
        } else if (_period == Billing.SubscriptionPeriod.Week) {
            _endTs = _startTs + (7 days) * uint256(_prepaidPeriods);
        } else if (_period == Billing.SubscriptionPeriod.Month) {
            _endTs = _startTs + (30 days) * uint256(_prepaidPeriods);
        } else if (_period == Billing.SubscriptionPeriod.Year) {
            _endTs = _startTs + (365 days) * uint256(_prepaidPeriods);
        } else {
            revert("Invalid subscription period");
        }

        require((_endTs - _startTs) < 366 days, "subscriptions over 365 days are not allowed");

        // Store subscription
        subscriptions[customer][_tokenId].startTs = _startTs;
        subscriptions[customer][_tokenId].endTs = _endTs;
        subscriptions[customer][_tokenId].period = _period;
        subscriptions[customer][_tokenId].currencySymbol = _currencySymbol;
        subscriptions[customer][_tokenId].priceRate = _priceRate;
        subscriptions[customer][_tokenId].vatRate = _vatRate;

        nodeidFreeAfter[_tokenId] = _endTs;

        // Transfer amount to destinationWallet
        require(
            token.transferFrom(_msgSender(), destinationWallet, totalTokenAmount),
            "error occurred during transferFrom"
        );

        // Refund NFT caution
        if (nodeIdToken.getClaimableBalanceOf(customer) > 0) nodeIdToken.refund(payable(customer));

        // Remove one validation slot
        validationSlots--;

        emit NewSubscription(
            customer,
            _tokenId,
            _period,
            _prepaidPeriods,
            _currencySymbol,
            _startTs,
            _endTs,
            usdPriceForSubscriptionExclTax,
            usdTax,
            _priceRate,
            _vatRate
        );
    }

    /// @notice Allows a user to reload an existing subscription
    /// @param _tokenId The Nodeid NFT identifier
    /// @param _addedPeriods The number of added periods
    /// @dev Only allowed on the last period
    function reloadSubscription(uint256 _tokenId, uint16 _addedPeriods) public {
        // Check that customer is already registered and has accepted latest GTC version
        require(customerExists(_msgSender()), "not a known customer");
        require(customers[_msgSender()].acceptedGtcVersion == gtcVersion, "You must accept our new GTC");

        require(
            billing.isDeniedCountry(customers[_msgSender()].countryOfResidence) == false,
            "Your country of residence is denied by our policy"
        );

        // Nb prepaid periods must be > 0
        require(_addedPeriods > 0, "nb of prepaid periods must be at least 1");

        require(nodeIdToken.ownerOf(_tokenId) == _msgSender(), "not owner of nodeId");

        require(
            keccak256(abi.encodePacked(nodeIdToken.nodeIds(_tokenId))) != keccak256(abi.encodePacked("")),
            "nodeId is void"
        );

        require(hasActiveSubscription(_msgSender(), _tokenId), "no active subscription for this node id");

        require(hasNodeIdCurrentSubscription(_tokenId), "nodeid has no subscription");

        Subscription memory currentSubscription = subscriptions[_msgSender()][_tokenId];

        // Check that currency is supported
        require(billing.isSupportedCurrency(currentSubscription.currencySymbol), "not a supported currency");

        uint256 usdPriceForSubscriptionExclTax = billing.usdPriceExcludingTax(
            currentSubscription.period,
            _addedPeriods
        );
        uint256 usdTax = billing.taxAmount(usdPriceForSubscriptionExclTax, customers[_msgSender()].countryOfResidence);

        uint256 totalTokenAmount = billing.usdToCurrency(
            usdPriceForSubscriptionExclTax + usdTax,
            currentSubscription.currencySymbol
        );

        // Check that Smart Contract is allowed to transfer total amount
        IERC20Upgradeable token = IERC20Upgradeable(billing.currencies(currentSubscription.currencySymbol));
        require(token.allowance(_msgSender(), address(this)) >= totalTokenAmount, "not enough allowance");

        /* solhint-disable-next-line not-rely-on-time  */
        uint256 _startTs = currentSubscription.endTs;
        uint256 _endTs = currentSubscription.endTs;

        if (currentSubscription.period == Billing.SubscriptionPeriod.Day) {
            require(
                computeRefundPeriodsOf(_tokenId, _msgSender()) < 8,
                "a daily subscription can only be reloaded in the last week"
            );
            _endTs = _startTs + (1 days) * uint256(_addedPeriods);
            require((_endTs - _startTs) < 366 days, "a weekly subscription reload can't exceed one year");
        } else if (currentSubscription.period == Billing.SubscriptionPeriod.Week) {
            require(
                computeRefundPeriodsOf(_tokenId, _msgSender()) == 0,
                "a weekly subscription can only be reloaded in the last week"
            );
            _endTs = _startTs + (7 days) * uint256(_addedPeriods);
            require((_endTs - _startTs) < 366 days, "a weekly subscription reload can't exceed one year");
        } else if (currentSubscription.period == Billing.SubscriptionPeriod.Month) {
            require(
                computeRefundPeriodsOf(_tokenId, _msgSender()) == 0,
                "a monthly subscription can only be reloaded in the last month"
            );
            _endTs = _startTs + (30 days) * uint256(_addedPeriods);
            require((_endTs - _startTs) < 366 days, "a monthly subscription reload can't exceed one year");
        } else if (currentSubscription.period == Billing.SubscriptionPeriod.Year) {
            _endTs = _startTs + (365 days) * uint256(_addedPeriods);
            require(
                /* solhint-disable-next-line not-rely-on-time  */
                (_endTs - block.timestamp) < 396 days,
                "a yearly subscription can only be reloaded the last month and only for one more year"
            );
        } else {
            revert("Invalid subscription period");
        }

        // Transfer amount to destinationWallet
        require(
            token.transferFrom(_msgSender(), destinationWallet, totalTokenAmount),
            "error occurred during transferFrom"
        );

        // Store subscription
        uint256 _priceRate = billing.priceRates(uint8(currentSubscription.period));
        uint256 _vatRate = billing.vatRates(customers[_msgSender()].countryOfResidence);

        subscriptions[_msgSender()][_tokenId].endTs = _endTs;
        subscriptions[_msgSender()][_tokenId].priceRate = _priceRate;
        subscriptions[_msgSender()][_tokenId].vatRate = _vatRate;

        nodeidFreeAfter[_tokenId] = _endTs;

        emit ReloadSubscription(
            _msgSender(),
            _tokenId,
            currentSubscription.period,
            _addedPeriods,
            currentSubscription.currencySymbol,
            currentSubscription.startTs,
            _endTs,
            usdPriceForSubscriptionExclTax,
            usdTax,
            _priceRate,
            _vatRate
        );
    }

    function stopSubscription(uint256 _tokenId) public {
        _stopSubscription(_tokenId, _msgSender());
    }

    function stopSubscription(uint256 _tokenId, address customer) public onlyAdmin {
        _stopSubscription(_tokenId, customer);
    }

    /// @notice Allows user to stop/terminate an existing subscription. Unused periods will be refunded
    /// @param  _tokenId The Nodeid NFT identifier
    function _stopSubscription(uint256 _tokenId, address customer) private {
        // Check that customer has an active subscription for this nodeId
        require(hasActiveSubscription(customer, _tokenId) == true, "no active subscription for this node id");
        require(
            hasPendingRefund(customer, _tokenId) == false,
            "a pending refund exists for this node id, can't stop this subscription"
        );

        // Compute amount of periods to refund
        Subscription memory currentSubscription = subscriptions[customer][_tokenId];

        /* solhint-disable-next-line not-rely-on-time  */
        uint256 refundPeriods = computeRefundPeriodsOf(_tokenId, customer);

        if (refundPeriods > 0) {
            uint256 refundUsdPriceExclTax = currentSubscription.priceRate * refundPeriods;
            uint256 usdTax = refundUsdPriceExclTax * currentSubscription.vatRate;

            if (usdTax % 100 >= 50) usdTax = usdTax / 100 + 1;
            else usdTax = usdTax / 100;

            refunds[customer][_tokenId].pending = true;
            refunds[customer][_tokenId].currencySymbol = currentSubscription.currencySymbol;
            refunds[customer][_tokenId].amount = refundUsdPriceExclTax + usdTax;

            emit NewRefund(
                customer,
                _tokenId,
                currentSubscription.startTs,
                currentSubscription.currencySymbol,
                refunds[customer][_tokenId].amount,
                refundPeriods
            );
        }
        // else no refund if no period to refund!
        /* solhint-disable-next-line not-rely-on-time  */
        nodeidFreeAfter[_tokenId] = block.timestamp;

        // Add an available validation slot
        validationSlots++;

        emit StopSubscription(
            customer,
            _tokenId,
            subscriptions[customer][_tokenId].startTs,
            subscriptions[customer][_tokenId].endTs,
            /* solhint-disable-next-line not-rely-on-time  */
            block.timestamp
        );

        /* solhint-disable-next-line not-rely-on-time  */
        subscriptions[customer][_tokenId].endTs = block.timestamp;
    }

    /// @notice Queries if a user is registered
    /// @param _wallet Customer's wallet address
    /// @return boolean True if the user is registered, otherwise false.
    function customerExists(address _wallet) public view returns (bool) {
        return customers[_wallet].registered;
    }

    /// @notice Queries whether a nodeid is used in an active subscription
    /// @param _tokenId The Nodeid NFT identifier
    /// @return boolean True if the NodeID as an active subscription, otherwise false
    function hasNodeIdCurrentSubscription(uint256 _tokenId) public view returns (bool) {
        /* solhint-disable-next-line not-rely-on-time  */
        return nodeidFreeAfter[_tokenId] > block.timestamp;
    }

    /// @notice Queries if a user has an active subscription for the given nft identifier
    /// @param _wallet Customer's wallet address
    /// @param _tokenId The Nodeid NFT identifier
    /// @return boolean True if the NodeID as an active subscription, otherwise false
    function hasActiveSubscription(address _wallet, uint256 _tokenId) public view returns (bool) {
        require(customerExists(_wallet), "Not a known customer");
        return
            /* solhint-disable-next-line not-rely-on-time  */
            subscriptions[_wallet][_tokenId].startTs != 0 && subscriptions[_wallet][_tokenId].endTs > block.timestamp;
    }

    /// @notice Queries if a user has a pending refund for the given nft identifier
    /// @param _wallet Customer's wallet address
    /// @param _tokenId The Nodeid NFT identifier
    /// @return boolean True if the is a pending refund for the given user's nft
    function hasPendingRefund(address _wallet, uint256 _tokenId) public view returns (bool) {
        return refunds[_wallet][_tokenId].pending == true;
    }

    /// @notice Computes how many periods must be refund for a given subscription
    /// @dev _tokenId and _user are used to query the subscription
    /// @param _tokenId The Nodeid NFT identifier
    /// @param _user Customer's wallet address
    /// @return refundPeriods Returns the number of periods to be refunded
    function computeRefundPeriodsOf(uint256 _tokenId, address _user) public view returns (uint256) {
        // Compute amount of periods to refund
        Subscription memory currentSubscription = subscriptions[_user][_tokenId];

        /* solhint-disable-next-line not-rely-on-time  */
        uint256 remainingSec = currentSubscription.endTs - block.timestamp;
        uint256 refundPeriods = 0;
        if (currentSubscription.period == Billing.SubscriptionPeriod.Day) {
            refundPeriods = remainingSec / 1 days;
        } else if (currentSubscription.period == Billing.SubscriptionPeriod.Week) {
            refundPeriods = remainingSec / 7 days;
        } else if (currentSubscription.period == Billing.SubscriptionPeriod.Month) {
            refundPeriods = remainingSec / 30 days;
        } else if (currentSubscription.period == Billing.SubscriptionPeriod.Year) {
            refundPeriods = remainingSec / 365 days;
        } else {
            revert("Invalid subscription period");
        }

        return refundPeriods;
    }
}
