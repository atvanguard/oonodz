# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

**Foreword:** we adhere to the semantic version despite the fact that we do not offer an API in the REST API sense of the term. Smartcontracts offer an ABI interface that must not be broken.

## [Unreleased]

## [2.0.0] - 2023-11-08

### Added

- Add a daily plan to allow customers to subscribe for a period shorter than a week.
- Adds a wrapper allowing specified integrator contracts to subscribe a plan for a customer.
- Add a method to decode NodeID to cb58 address.
- Allows administrators to stop a subscription in progress.

## [1.0.1] - 2023-09-26
### Fixed

- Deletion of the if statement, which can lead to a user's refund being removed if he or she cancels his or her subscription twice within a short period of time.
- Resolve an issue regarding yearly top-up
- Potential Re-Entrancy vulnerability in newSubscription: Move the transferFrom call after state updates to avoid exposure to Re-Entrancy
- Invert state update of refunds[_wallet][_tokenId].pending and refund transferFrom to avoid Re-Entrancy in the case of ERC20 token with an external call
- Returned value of transferFrom was not used

## [1.0.0] - 2023-05-03

### Added

- First version containing avalanche validator sales and node ID management
- Compliance with European regulations on VAT collection
- Compliance with European regulations concerning countries not authorized to trade
- Verified contracts on snowscan.io and testnet.snowscan.io