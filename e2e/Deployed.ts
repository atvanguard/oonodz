import { ethers } from "hardhat";
import { expect } from "chai";
import { readFileSync, promises as fsPromises } from 'fs';
import { resolve } from 'path';

import { DEPLOY_CONFIGS } from "../deploy-config";


const MONTBLANC_CONFIG = DEPLOY_CONFIGS["montblanc"];

const dir = resolve(__dirname, "../addr.json");
const file = readFileSync(dir, "utf8");
const json = JSON.parse(file);

const ACCESS_CONTROL_ADDRESS = json.AccessControl;
const BILLING_ADDRESS = json.Billing;
const NODEIDNFT_ADDRESS = json.NodeIdNFT;
const SUBSCRIPTIONS_ADDRESS = json.Subscriptions;

const USDC_ADDRESS = MONTBLANC_CONFIG.tokenAddresses.usdc;
const USDT_ADDRESS = MONTBLANC_CONFIG.tokenAddresses.usdt;

describe("Deployed Contracts", function () {
  async function getDeployedContracts() {
    const AccessControl = await ethers.getContractFactory("AccessControl");
    const access = AccessControl.attach(ACCESS_CONTROL_ADDRESS);

    const roles = {
      ADMIN: await access.ADMIN_ROLE(),
      BURNER: await access.BURNER_ROLE(),
      MINTER: await access.MINTER_ROLE(),
      BILLING: await access.BILLING_ROLE(),
      REFUNDER: await access.REFUNDER_ROLE(),
    };

    const nicolasAddr = {
      minter: "0xBFea51610d176d14f6D548D28daFEbbb650A5e71",
      subscriber: [
        "0xF27b53E79703196cBC73AA5661C5d97445548740",
        "0xAaFD9b644BDAa554231260Cd13ffE93d98E23DAF",
        "0x583948F1F45aF5e9184Ce4273482a6e490BA72D9",
        "0x5a6d686F984Ca64c7853d1EA9f6e97Df7A19EcDC",
        "0x285172d61D30Fe1fc828cB2063626504d57cD481",
        "0x436Ad64A9ba824BEa3ee8C11D69B37c9cdFa0a75",
        "0x594798211aAdaE61C89088f9C3fb2B00E44131e9",
        "0x03ef837801FE5c6bE9362B6415AC277A4Bb14707",
        "0xDdf752d68Ce0ECf70B305Db2C38B900d79A81335",
      ]
    };

    const Billing = await ethers.getContractFactory("Billing");
    const billingContract = Billing.attach(BILLING_ADDRESS);

    const NFT = await ethers.getContractFactory("NodeIdNFT");
    const nft = NFT.attach(NODEIDNFT_ADDRESS);

    const Subscriptions = await ethers.getContractFactory("Subscriptions");
    const subscriptions = Subscriptions.attach(SUBSCRIPTIONS_ADDRESS);

    // const usdtToken = await ethers.getContractAt("IERC20Upgradeable", USDT_ADDRESS);
    const usdtToken = await ethers.getContractAt("ERC20Upgradeable", USDT_ADDRESS);
    const usdcToken = await ethers.getContractAt("ERC20Upgradeable", USDC_ADDRESS);

    const accounts = await ethers.getSigners();

    const cautionPrice = await nft.cautionPrice();

    let default_nodeids: string[] = ["7Xhw2mDxuDS44j42TCB6U5579esbSt3Lg", "MFrZFVCXPv5iCn6M9K6XduxGTYp891xXZ", "NFBbbJ4qCmNaCzeW7sxErhvWqvEQMnYcN", "GWPcbFJZFfZreETSoWjPimr846mXEKCtu", "P7oB2McjBGgW2NXXWVYjV8JEDFoW9xDE5"];

    const deployer = accounts[0];

    await deployer.sendTransaction({
      to: accounts[1].address,
      value: ethers.utils.parseEther("5.0")
    });

    await deployer.sendTransaction({
      to: accounts[5].address,
      value: ethers.utils.parseEther("5.0")
    });

    await deployer.sendTransaction({
      to: accounts[3].address,
      value: ethers.utils.parseEther("5.0")
    });

    await deployer.sendTransaction({
      to: accounts[7].address,
      value: ethers.utils.parseEther("5.0")
    });

    return {
      access,
      billingContract,
      subscriptions,
      nft,
      roles,
      usdtToken,
      usdcToken,
      signers: {
        deployer: accounts[0],
        admin: accounts[1],
        minter: accounts[5],
        burner: accounts[3],
        refunder: accounts[7],
      },
      cautionPrice,
      default_nodeids,
      nicolasAddr,
      deployer
    };
  }

  describe("Configure nicolas's wallets", function () {
    it("Fund refund's wallet", async function () {
      const { deployer, usdcToken, usdtToken, signers } = await getDeployedContracts();

      const usdcDecimals = await usdcToken.decimals();
      const usdtDecimals = await usdtToken.decimals();

      await usdcToken.connect(deployer).transfer(signers.refunder.address, ethers.utils.parseUnits("20000", usdcDecimals));
      await usdtToken.connect(deployer).transfer(signers.refunder.address, ethers.utils.parseUnits("20000", usdtDecimals));
    });

    it("Found Nicolas's wallets", async function () {
      const { deployer, nicolasAddr, usdcToken, usdtToken } = await getDeployedContracts();

      await deployer.sendTransaction({
        to: nicolasAddr.minter,
        value: ethers.utils.parseEther("5.0")
      });

      const usdcDecimals = await usdcToken.decimals();
      const usdtDecimals = await usdtToken.decimals();

      for (var sub of nicolasAddr.subscriber) {
        await deployer.sendTransaction({
          to: sub,
          value: ethers.utils.parseEther("5.0")
        });

        await usdcToken.connect(deployer).transfer(sub, ethers.utils.parseUnits("2000", usdcDecimals));
        await usdtToken.connect(deployer).transfer(sub, ethers.utils.parseUnits("2000", usdtDecimals));
      }
    });
  });

  describe("Access Control", function () {
    it("should have valid accounts", async function () {
      const { access, roles, nicolasAddr } = await getDeployedContracts();
      expect(await access.hasRole(roles.ADMIN, MONTBLANC_CONFIG.accounts.admin)).to.equals(true);
      expect(await access.hasRole(roles.BILLING, MONTBLANC_CONFIG.accounts.billing)).to.equals(true);
      expect(await access.hasRole(roles.BURNER, MONTBLANC_CONFIG.accounts.burner)).to.equals(true);
      expect(await access.hasRole(roles.MINTER, MONTBLANC_CONFIG.accounts.minter)).to.equals(true);
      expect(await access.hasRole(roles.REFUNDER, MONTBLANC_CONFIG.accounts.refunder)).to.equals(true);
    });
  });

  describe("Billing", function () {
    it("should have valid configuration", async function () {
      const { billingContract } = await getDeployedContracts();

      expect(await billingContract.currencies("USDT")).to.equals(
        USDT_ADDRESS,
        "Invalid configuration: USDT contract is not correct"
      );
      expect(await billingContract.currencies("USDC")).to.equals(
        USDC_ADDRESS,
        "Invalid configuration: USDC contract is not correct"
      );
      expect(await billingContract.currencies("DAI")).to.equals("0x0000000000000000000000000000000000000000");
      expect(await billingContract.vatRates(250)).to.equals(20);
      expect(await billingContract.vatRates(442)).to.equals(16);
      expect(await billingContract.vatRates(380)).to.equals(22);
    });
  });

  describe("NodeIdNFT", function () {
    it("should allow minter to mint a token", async function () {
      // signers.minter & signers.burner must have funds to run these functions
      const { nft, signers, cautionPrice } = await getDeployedContracts();
      // Check that token is not owned
      await expect(nft.ownerOf(1234)).to.be.revertedWith("ERC721: invalid token ID");
      // Minter mints token 1234 to admin wallet
      await (await nft.connect(signers.minter)["preMint(uint256)"](1234)).wait();
      expect(await nft.ownerOf(1234)).to.equals(signers.minter.address);
      // Mint toen prior burning it (otherwise token will stay as a available preMint token)
      await (await nft.connect(signers.minter).deposit({ value: cautionPrice })).wait();
      await (await nft.connect(signers.minter).mintBlank()).wait();
      expect(await nft.ownerOf(1234)).to.equals(signers.minter.address);
      // Burner burns token 1234
      await (await nft.connect(signers.burner).burn(1234)).wait();
      // Token 1234 should be free again
      await expect(nft.ownerOf(1234)).to.be.revertedWith("ERC721: invalid token ID");
    });
  });

  describe("Deploy testnet default nodeids and subscriptions", function () {
    it("Mint default node ids", async function () {
      this.timeout(300000);

      const { nft, signers, cautionPrice, default_nodeids, subscriptions, billingContract, usdcToken } = await getDeployedContracts();

      // Add 5 slots by defaults
      await subscriptions.connect(signers.minter).addValidationSlots(5);

      // Register admin user
      await subscriptions.connect(signers.admin)["register(uint16)"](250);

      let i: number = 1;

      for (var nodeid of default_nodeids) {

        console.log(`Deploying id: ${i}; nodeid: ${nodeid}`);
        await (await nft.connect(signers.minter)["preMint(uint256,string)"](i, nodeid)).wait();

        await (await nft.connect(signers.admin).deposit({ value: cautionPrice })).wait();
        await (await nft.connect(signers.admin)["mint()"]()).wait();

        i++;
      }

      const count_nft = await nft.balanceOf(signers.admin.address);
      const amount = await billingContract.usdPriceExcludingTax(0, 52);
      const taxAmount = await billingContract.taxAmount(amount, 250);
      const total = await billingContract.usdToCurrency(amount + taxAmount, "USDC");

      for (let i = 0; i < count_nft; i++) {
        const tokenid = await nft.tokenOfOwnerByIndex(signers.admin.address, i);
        const nodeid = await nft.nodeIds(tokenid);

        console.log(`Subscribing:
        - TokenID: ${tokenid}
        - NodeID: ${nodeid}
        - amount: ${amount}
        - taxAmount: ${taxAmount}`);

        await (await usdcToken.connect(signers.admin).approve(subscriptions.address, total)).wait();

        await (await subscriptions.connect(signers.admin)["newSubscription(uint8,uint256,string,uint16,bool)"](0, tokenid, "USDC", 52, true)).wait();
      }
    });
  });
});
